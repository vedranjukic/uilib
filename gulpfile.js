// include gulp
var gulp = require('gulp'); 
 
// include plug-ins
var less = require('gulp-less');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var yuidoc = require("gulp-yuidoc");
var traceur = require("gulp-traceur");

gulp.task('build-less', function(){
    return gulp.src('./less/base.less')
        .pipe(less())
        .pipe(gulp.dest('./css'));
});

gulp.task('combineVendorJS', function() {
  return gulp.src('./vendor-libs/**/*.js')
  	.pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
  	.pipe(sourcemaps.write())
    .pipe(gulp.dest('./js/'));
});

gulp.task('combineVendorCSS', function() {
  return gulp.src('./vendor-libs/**/*.css')
  	.pipe(sourcemaps.init())
    .pipe(concat('vendor.css'))
  	.pipe(sourcemaps.write())
    .pipe(gulp.dest('./css/'));
});

gulp.task('yuidoc', function(){
	return gulp.src('./lib/**/*.js')
  	.pipe(sourcemaps.init())
	.pipe(yuidoc())
	.pipe(gulp.dest("./doc"));
});

gulp.task('compile', function () {
    return gulp.src('ui.js')
        .pipe(traceur())
        .pipe(concat('ui.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('bundle', function() {
	return gulp.src(['./js/vendor.js', './dist/ui.js'])
    .pipe(concat('ui-all.js'))
    .pipe(gulp.dest('./dist'));
});

// default gulp task
gulp.task('default', ['build-less', 'combineVendorJS', 'combineVendorCSS', 'yuidoc', 'compile', 'bundle'], function() {
});