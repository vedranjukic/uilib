"use strict";
Object.defineProperties(module.exports, {
  CAWebUI: {get: function() {
      return CAWebUI;
    }},
  __esModule: {value: true}
});
var $__lib_47_button_47_button_46_js__,
    $__lib_47_container_47_container_46_js__,
    $__lib_47_form_47_input_47_combobox_46_js__,
    $__lib_47_component_46_js__,
    $__lib_47_form_47_componentFormField_46_js__,
    $__lib_47_componentFocus_46_js__,
    $__lib_47_componentManager_46_js__,
    $__lib_47_data_47_dataset_46_js__,
    $__lib_47_form_47_fieldset_46_js__,
    $__lib_47_form_47_fieldvalidation_46_js__,
    $__lib_47_form_47_formpanel_46_js__,
    $__lib_47_basic_47_htmlframe_46_js__,
    $__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__,
    $__lib_47_form_47_input_47_input_46_js__,
    $__lib_47_layout_47_layout_46_js__,
    $__lib_47_layout_47_layouts_47_absolute_46_js__,
    $__lib_47_layout_47_layouts_47_accordion_46_js__,
    $__lib_47_layout_47_layouts_47_auto_46_js__,
    $__lib_47_layout_47_layouts_47_border_46_js__,
    $__lib_47_layout_47_layouts_47_column_46_js__,
    $__lib_47_layout_47_layouts_47_fieldset_46_js__,
    $__lib_47_layout_47_layouts_47_fit_46_js__,
    $__lib_47_layout_47_layouts_47_hbox_46_js__,
    $__lib_47_layout_47_layouts_47_vbox_46_js__,
    $__lib_47_layout_47_layouts_47_page_46_js__,
    $__lib_47_layout_47_layouts_47_tab_46_js__,
    $__lib_47_container_47_multisplit_46_js__,
    $__lib_47_panel_47_panel_46_js__,
    $__lib_47_panel_47_pagepanel_46_js__,
    $__lib_47_panel_47_tabpanel_46_js__,
    $__lib_47_tree_47_treepanel_46_js__,
    $__lib_47_container_47_viewport_46_js__,
    $__lib_47_window_47_window_46_js__;
var Button = ($__lib_47_button_47_button_46_js__ = require("./lib/button/button.js"), $__lib_47_button_47_button_46_js__ && $__lib_47_button_47_button_46_js__.__esModule && $__lib_47_button_47_button_46_js__ || {default: $__lib_47_button_47_button_46_js__}).Button;
var Container = ($__lib_47_container_47_container_46_js__ = require("./lib/container/container.js"), $__lib_47_container_47_container_46_js__ && $__lib_47_container_47_container_46_js__.__esModule && $__lib_47_container_47_container_46_js__ || {default: $__lib_47_container_47_container_46_js__}).Container;
var ComboBox = ($__lib_47_form_47_input_47_combobox_46_js__ = require("./lib/form/input/combobox.js"), $__lib_47_form_47_input_47_combobox_46_js__ && $__lib_47_form_47_input_47_combobox_46_js__.__esModule && $__lib_47_form_47_input_47_combobox_46_js__ || {default: $__lib_47_form_47_input_47_combobox_46_js__}).ComboBox;
var Component = ($__lib_47_component_46_js__ = require("./lib/component.js"), $__lib_47_component_46_js__ && $__lib_47_component_46_js__.__esModule && $__lib_47_component_46_js__ || {default: $__lib_47_component_46_js__}).Component;
var ComponentFormField = ($__lib_47_form_47_componentFormField_46_js__ = require("./lib/form/componentFormField.js"), $__lib_47_form_47_componentFormField_46_js__ && $__lib_47_form_47_componentFormField_46_js__.__esModule && $__lib_47_form_47_componentFormField_46_js__ || {default: $__lib_47_form_47_componentFormField_46_js__}).ComponentFormField;
var ComponentFocus = ($__lib_47_componentFocus_46_js__ = require("./lib/componentFocus.js"), $__lib_47_componentFocus_46_js__ && $__lib_47_componentFocus_46_js__.__esModule && $__lib_47_componentFocus_46_js__ || {default: $__lib_47_componentFocus_46_js__}).ComponentFocus;
var ComponentManager = ($__lib_47_componentManager_46_js__ = require("./lib/componentManager.js"), $__lib_47_componentManager_46_js__ && $__lib_47_componentManager_46_js__.__esModule && $__lib_47_componentManager_46_js__ || {default: $__lib_47_componentManager_46_js__}).ComponentManager;
var DataSet = ($__lib_47_data_47_dataset_46_js__ = require("./lib/data/dataset.js"), $__lib_47_data_47_dataset_46_js__ && $__lib_47_data_47_dataset_46_js__.__esModule && $__lib_47_data_47_dataset_46_js__ || {default: $__lib_47_data_47_dataset_46_js__}).DataSet;
var FieldSet = ($__lib_47_form_47_fieldset_46_js__ = require("./lib/form/fieldset.js"), $__lib_47_form_47_fieldset_46_js__ && $__lib_47_form_47_fieldset_46_js__.__esModule && $__lib_47_form_47_fieldset_46_js__ || {default: $__lib_47_form_47_fieldset_46_js__}).FieldSet;
var FieldValidation = ($__lib_47_form_47_fieldvalidation_46_js__ = require("./lib/form/fieldvalidation.js"), $__lib_47_form_47_fieldvalidation_46_js__ && $__lib_47_form_47_fieldvalidation_46_js__.__esModule && $__lib_47_form_47_fieldvalidation_46_js__ || {default: $__lib_47_form_47_fieldvalidation_46_js__}).FieldValidation;
var FormPanel = ($__lib_47_form_47_formpanel_46_js__ = require("./lib/form/formpanel.js"), $__lib_47_form_47_formpanel_46_js__ && $__lib_47_form_47_formpanel_46_js__.__esModule && $__lib_47_form_47_formpanel_46_js__ || {default: $__lib_47_form_47_formpanel_46_js__}).FormPanel;
var HtmlFrame = ($__lib_47_basic_47_htmlframe_46_js__ = require("./lib/basic/htmlframe.js"), $__lib_47_basic_47_htmlframe_46_js__ && $__lib_47_basic_47_htmlframe_46_js__.__esModule && $__lib_47_basic_47_htmlframe_46_js__ || {default: $__lib_47_basic_47_htmlframe_46_js__}).HtmlFrame;
var HorizontalMenu = ($__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__ = require("./lib/menu/horizontal/horizontalMenu.js"), $__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__ && $__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__.__esModule && $__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__ || {default: $__lib_47_menu_47_horizontal_47_horizontalMenu_46_js__}).HorizontalMenu;
var Input = ($__lib_47_form_47_input_47_input_46_js__ = require("./lib/form/input/input.js"), $__lib_47_form_47_input_47_input_46_js__ && $__lib_47_form_47_input_47_input_46_js__.__esModule && $__lib_47_form_47_input_47_input_46_js__ || {default: $__lib_47_form_47_input_47_input_46_js__}).Input;
var Layout = ($__lib_47_layout_47_layout_46_js__ = require("./lib/layout/layout.js"), $__lib_47_layout_47_layout_46_js__ && $__lib_47_layout_47_layout_46_js__.__esModule && $__lib_47_layout_47_layout_46_js__ || {default: $__lib_47_layout_47_layout_46_js__}).Layout;
var LayoutAbsolute = ($__lib_47_layout_47_layouts_47_absolute_46_js__ = require("./lib/layout/layouts/absolute.js"), $__lib_47_layout_47_layouts_47_absolute_46_js__ && $__lib_47_layout_47_layouts_47_absolute_46_js__.__esModule && $__lib_47_layout_47_layouts_47_absolute_46_js__ || {default: $__lib_47_layout_47_layouts_47_absolute_46_js__}).LayoutAbsolute;
var LayoutAccordion = ($__lib_47_layout_47_layouts_47_accordion_46_js__ = require("./lib/layout/layouts/accordion.js"), $__lib_47_layout_47_layouts_47_accordion_46_js__ && $__lib_47_layout_47_layouts_47_accordion_46_js__.__esModule && $__lib_47_layout_47_layouts_47_accordion_46_js__ || {default: $__lib_47_layout_47_layouts_47_accordion_46_js__}).LayoutAccordion;
var LayoutAuto = ($__lib_47_layout_47_layouts_47_auto_46_js__ = require("./lib/layout/layouts/auto.js"), $__lib_47_layout_47_layouts_47_auto_46_js__ && $__lib_47_layout_47_layouts_47_auto_46_js__.__esModule && $__lib_47_layout_47_layouts_47_auto_46_js__ || {default: $__lib_47_layout_47_layouts_47_auto_46_js__}).LayoutAuto;
var LayoutBorder = ($__lib_47_layout_47_layouts_47_border_46_js__ = require("./lib/layout/layouts/border.js"), $__lib_47_layout_47_layouts_47_border_46_js__ && $__lib_47_layout_47_layouts_47_border_46_js__.__esModule && $__lib_47_layout_47_layouts_47_border_46_js__ || {default: $__lib_47_layout_47_layouts_47_border_46_js__}).LayoutBorder;
var LayoutColumn = ($__lib_47_layout_47_layouts_47_column_46_js__ = require("./lib/layout/layouts/column.js"), $__lib_47_layout_47_layouts_47_column_46_js__ && $__lib_47_layout_47_layouts_47_column_46_js__.__esModule && $__lib_47_layout_47_layouts_47_column_46_js__ || {default: $__lib_47_layout_47_layouts_47_column_46_js__}).LayoutColumn;
var LayoutFieldSet = ($__lib_47_layout_47_layouts_47_fieldset_46_js__ = require("./lib/layout/layouts/fieldset.js"), $__lib_47_layout_47_layouts_47_fieldset_46_js__ && $__lib_47_layout_47_layouts_47_fieldset_46_js__.__esModule && $__lib_47_layout_47_layouts_47_fieldset_46_js__ || {default: $__lib_47_layout_47_layouts_47_fieldset_46_js__}).LayoutFieldSet;
var LayoutFit = ($__lib_47_layout_47_layouts_47_fit_46_js__ = require("./lib/layout/layouts/fit.js"), $__lib_47_layout_47_layouts_47_fit_46_js__ && $__lib_47_layout_47_layouts_47_fit_46_js__.__esModule && $__lib_47_layout_47_layouts_47_fit_46_js__ || {default: $__lib_47_layout_47_layouts_47_fit_46_js__}).LayoutFit;
var LayoutHBox = ($__lib_47_layout_47_layouts_47_hbox_46_js__ = require("./lib/layout/layouts/hbox.js"), $__lib_47_layout_47_layouts_47_hbox_46_js__ && $__lib_47_layout_47_layouts_47_hbox_46_js__.__esModule && $__lib_47_layout_47_layouts_47_hbox_46_js__ || {default: $__lib_47_layout_47_layouts_47_hbox_46_js__}).LayoutHBox;
var LayoutVBox = ($__lib_47_layout_47_layouts_47_vbox_46_js__ = require("./lib/layout/layouts/vbox.js"), $__lib_47_layout_47_layouts_47_vbox_46_js__ && $__lib_47_layout_47_layouts_47_vbox_46_js__.__esModule && $__lib_47_layout_47_layouts_47_vbox_46_js__ || {default: $__lib_47_layout_47_layouts_47_vbox_46_js__}).LayoutVBox;
var LayoutPage = ($__lib_47_layout_47_layouts_47_page_46_js__ = require("./lib/layout/layouts/page.js"), $__lib_47_layout_47_layouts_47_page_46_js__ && $__lib_47_layout_47_layouts_47_page_46_js__.__esModule && $__lib_47_layout_47_layouts_47_page_46_js__ || {default: $__lib_47_layout_47_layouts_47_page_46_js__}).LayoutPage;
var LayoutTab = ($__lib_47_layout_47_layouts_47_tab_46_js__ = require("./lib/layout/layouts/tab.js"), $__lib_47_layout_47_layouts_47_tab_46_js__ && $__lib_47_layout_47_layouts_47_tab_46_js__.__esModule && $__lib_47_layout_47_layouts_47_tab_46_js__ || {default: $__lib_47_layout_47_layouts_47_tab_46_js__}).LayoutTab;
var Multisplit = ($__lib_47_container_47_multisplit_46_js__ = require("./lib/container/multisplit.js"), $__lib_47_container_47_multisplit_46_js__ && $__lib_47_container_47_multisplit_46_js__.__esModule && $__lib_47_container_47_multisplit_46_js__ || {default: $__lib_47_container_47_multisplit_46_js__}).Multisplit;
var Panel = ($__lib_47_panel_47_panel_46_js__ = require("./lib/panel/panel.js"), $__lib_47_panel_47_panel_46_js__ && $__lib_47_panel_47_panel_46_js__.__esModule && $__lib_47_panel_47_panel_46_js__ || {default: $__lib_47_panel_47_panel_46_js__}).Panel;
var PagePanel = ($__lib_47_panel_47_pagepanel_46_js__ = require("./lib/panel/pagepanel.js"), $__lib_47_panel_47_pagepanel_46_js__ && $__lib_47_panel_47_pagepanel_46_js__.__esModule && $__lib_47_panel_47_pagepanel_46_js__ || {default: $__lib_47_panel_47_pagepanel_46_js__}).PagePanel;
var TabPanel = ($__lib_47_panel_47_tabpanel_46_js__ = require("./lib/panel/tabpanel.js"), $__lib_47_panel_47_tabpanel_46_js__ && $__lib_47_panel_47_tabpanel_46_js__.__esModule && $__lib_47_panel_47_tabpanel_46_js__ || {default: $__lib_47_panel_47_tabpanel_46_js__}).TabPanel;
var TreePanel = ($__lib_47_tree_47_treepanel_46_js__ = require("./lib/tree/treepanel.js"), $__lib_47_tree_47_treepanel_46_js__ && $__lib_47_tree_47_treepanel_46_js__.__esModule && $__lib_47_tree_47_treepanel_46_js__ || {default: $__lib_47_tree_47_treepanel_46_js__}).TreePanel;
var Viewport = ($__lib_47_container_47_viewport_46_js__ = require("./lib/container/viewport.js"), $__lib_47_container_47_viewport_46_js__ && $__lib_47_container_47_viewport_46_js__.__esModule && $__lib_47_container_47_viewport_46_js__ || {default: $__lib_47_container_47_viewport_46_js__}).Viewport;
var Window = ($__lib_47_window_47_window_46_js__ = require("./lib/window/window.js"), $__lib_47_window_47_window_46_js__ && $__lib_47_window_47_window_46_js__.__esModule && $__lib_47_window_47_window_46_js__ || {default: $__lib_47_window_47_window_46_js__}).Window;
var CAWebUI = function CAWebUI() {
  this.base = {
    Button: Button,
    Container: Container,
    Component: Component,
    ComponentFocus: ComponentFocus,
    ComponentManager: ComponentManager,
    Data: {DataSet: DataSet},
    Form: {
      ComboBox: ComboBox,
      ComponentFormField: ComponentFormField,
      FieldSet: FieldSet,
      FieldValidation: FieldValidation,
      FormPanel: FormPanel,
      Input: Input
    },
    HtmlFrame: HtmlFrame,
    Layout: Layout,
    Layouts: {
      Absolute: LayoutAbsolute,
      Accordion: LayoutAccordion,
      Auto: LayoutAuto,
      Border: LayoutBorder,
      Column: LayoutColumn,
      FieldSet: LayoutFieldSet,
      Fit: LayoutFit,
      HBox: LayoutHBox,
      VBox: LayoutVBox,
      Page: LayoutPage,
      Tab: LayoutTab
    },
    Menu: {HorizontalMenu: HorizontalMenu},
    Multisplit: Multisplit,
    Panel: Panel,
    PagePanel: PagePanel,
    TabPanel: TabPanel,
    TreePanel: TreePanel,
    Viewport: Viewport,
    Window: Window
  };
  this.componentManager = new ComponentManager(this);
};
($traceurRuntime.createClass)(CAWebUI, {}, {});
