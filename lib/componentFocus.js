import { Component } from './component.js';

/**
 * Extended Base class for all UI components that have focus ability.
 *
 * @class ComponentFocus
 * @extends Container
 */
export class ComponentFocus extends Component {
    
	/**
	 * @class ComponentFocus
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* On blur event handler
		*
		* @event onBlur
		*/
		this.onBlur = function(){};
		
		/**
		* On focus event handler
		*
		* @event onFocus
		*/
		this.onFocus = function(){};
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.onBlur && typeof params.onBlur != 'function') throw new Error("Not a valid function");
		if (params.onBlur) this.onBlur = params.onBlur;
		
		if (params.onFocus && typeof params.onFocus != 'function') throw new Error("Not a valid function");
		if (params.onFocus) this.onFocus = params.onFocus;
		
	}
	
	/**
	* Initialize component. Invoked automatically after rendering component to container parent.
	*
	* @method init
	*/
	init() {
		
		super.init();

		this.el.on('blur', this.onBlur);
		this.el.on('focus', this.onFocus);
		
	}
		
}