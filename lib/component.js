import { Base } from './base.js';

/**
 * Base class for all UI components.
 * The Component base class has built-in support for basic hide/show and enable/disable and size control behavior.
 * All Components are registered with the ComponentManager on construction so that they can be referenced at any time via CAWebUI.getComponent, passing the id.
 *
 * @class Component
 */
export class Component extends Base {
    
	/**
	 * @class Component
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		if (!params) params = {};

		if (!params.componentManager && !(CAWebUI && CAWebUI.componentManager)) throw new Error("ComponentManager is not optional");
		
		super(params);
		
		//	TODO: check class type
		//	if (params.componentManager.__proto__.toString() != "ComponentManager") throw new Error("Invalid ComponentManager object");
		
		/**
		* Component anchor {top,right,bottom,left} required by some Layout types
		* 
		* @property anchor
		* @type {string}
		* @default {top:false, right: false, bottom: false, left: false}
		*/
		this.anchor = {top:false, right: false, bottom: false, left: false};
		
		/**
		* Component has border
		* 
		* @property border
		* @type {bool}
		* @default false
		*/
		this.border = false;
		
		/**
		* Component css classes
		* 
		* @property class
		* @type {Set}
		*/
		this.class = new Set();
		
		/**
		* Is component disabled
		* 
		* @property disabled
		* @type {bool}
		* @default false
		*/
		this.disabled = false;
		
		/**
		* Column/row span used by some layouts(HBox,VBox)
		* 
		* @property flex
		* @type {Set}
		* @default 1
		*/
		this.flex = 1;
		
		/**
		* Component height in pixels
		* 
		* @property height
		* @type {Int}
		*/
		this.height = false;
		
		/**
		* Component layout
		* 
		* @property layout
		* @type {string} 
		*/
		this.layout = false;
		
		/**
		* Component left position
		* 
		* @property left
		* @type {int}
		*/
		this.left = false;
		
		/**
		* Component name
		* User assigned component name can be used later to get component from componentManager using getByName method.
		* 
		* @property name
		* @type {string}
		*/
		this.name = 'noname';	//	TODO: put some name generator function
		
		/**
		* Component region
		* Region will position component in some layouts: ["north", "east", "south", "west"]
		* 
		* @property region
		* @type {string}
		*/
		this.region = 'default';
		
		/**
		* Component css style
		* 
		* @property style
		* @type {Map} ["property","value"]
		*/
		this.style = new Map();
		
		/**
		* Component top position
		* 
		* @property top
		* @type {int}
		*/
		this.top = false;
		
		/**
		* Component width
		* 
		* @property width
		* @type {int}
		*/
		this.width = false;
		
		/**
		* On component pointer click event handler
		*
		* @event onClick
		*/
		this.onClick = function(){};
		
		/**
		* On component disabled event handler
		*
		* @event onDisable
		*/
		this.onDisable = function(){};
		
		/**
		* On component enabled event handler
		*
		* @event onEnable
		*/
		this.onEnable = function(){};
		
		this.componentManager = (params.componentManager)?params.componentManager:CAWebUI.componentManager;

		this.componentManager.register(this);
		
		return this;
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		if (params.anchor) this.anchor = params.anchor;
		if (params.border) this.border = params.border;
		if (params.class) this.class = params.class;
		if (params.disabled) this.disabled = params.disabled;
		if (params.flex) this.flex = params.flex;
		if (params.height) this.height = params.height;
		if (params.layout) this.layout = false;
		if (params.left) this.left = params.left;
		if (params.name) this.name = params.name;
		if (params.position) this.position = params.position;
		if (params.region !== undefined) this.region = params.region;
		if (params.top) this.top = params.top;
		if (params.width) this.width = params.width;
		
		if (params.onClick && typeof params.onClick != 'function') throw new Error("Not a valid function");
		if (params.onClick) this.onClick = params.onClick;
		
		if (params.onDisable && typeof params.onDisable != 'function') throw new Error("Not a valid function");
		if (params.onDisable) this.onDisable = params.onDisable;
		
		if (params.onEnable && typeof params.onEnable != 'function') throw new Error("Not a valid function");
		if (params.onEnable) this.onEnable = params.onEnable;
		
	}
	
	//
	//	property: left
	//	getters and setters
	//
	get left() {
		return this._left;
	}
	set left(newVal) {
		
		if (newVal === undefined || newVal === false) return;

		this._left = newVal;
		
		this.refresh();

	}
	
	//
	//	property: height
	//	getters and setters
	//
	get height() {
		return this._height;
	}
	set height(newVal) {
		
		if (newVal === undefined || newVal === false) return;

		this._height = newVal;
		
		this.refresh();

	}
	
	//
	//	property: top
	//	getters and setters
	//
	get top() {
		return this._top;
	}
	set top(newVal) {
		
		if (newVal === undefined || newVal === false) return;

		this._top= newVal;
		
		this.refresh();

	}
	
	//
	//	property: width
	//	getters and setters
	//
	get width() {
		return this._width;
	}
	set width(newVal) {
		
		if (newVal === undefined || newVal === false) return;

		this._width = newVal;
		
	}
	
	/**
	* Add css class to component
	*
	* @method addClass
	* @param {string} className Css class name
	*/
	addClass(className) {
		this.class.add(className);
		
		//	TODO: apply class if component is rendered
		
	}
	
	/**
	* Add css style to components style attribute
	*
	* @method addStyle
	* @param {string} property Css property name
	* @param {string} value Css property value
	*/
	addStyle(property, value) {
		
		this.style.set(property, value);
		
		if (this.el) this.applyStyle();
		
	}
	
	/**
	* Apply styles added to style set to component dom element
	*
	* @method applyStyle
	*/
	applyStyle() {
		
		this.el.attr('style', '');
		for (var [property, value] of this.style) {
			this.el.css(property, value);
		}
		
	}
	
	/**
	* Disable component
	*
	* @method disable
	*/
	disable() {
		
		this.disabled = true;
		this.refresh();
		this.onDisable();
	}
	
	/**
	* Enable component
	*
	* @method enable
	*/
	enable() {
		
		this.disabled = false;
		this.refresh();
		this.onEnable();
		
	}
	
	/**
	* Get Component Class Name. Usefull for extended classes.
	*
	* @method getTypeName
	*/
	getTypeName() {
		
		var funcNameRegex = /function (.{1,})\(/;
		var results = (funcNameRegex).exec((this).constructor.toString());
		return (results && results.length > 1) ? results[1] : "";
		
	}
	
	/**
	* Initialize component. Invoked automatically after rendering component to container parent.
	*
	* @method init
	*/
	init() {
		
	}
	
	/**
	* Refresh component
	*
	* @method refresh
	*/
	refresh() {
		
		if (!this.el) return false;
		
		if (this.height % 1 === 0) {
			this.addStyle('height', this.height + 'px');
		} else {
			this.addStyle('height', this.height);
		}
		
		if (this.left % 1 === 0) {
			this.addStyle('left', this.left + 'px');
		} else {
			this.addStyle('left', this.left);
		}
		
		if (this.top % 1 === 0) {
			this.addStyle('top', this.top + 'px');
		} else {
			this.addStyle('top', this.top);
		}
		
		if (this.width % 1 === 0) {
			this.addStyle('width', this.width + 'px');
		} else {
			this.addStyle('width', this.width);
		}
		
	}
	
	/**
	* Remove css class from component
	*
	* @method removeClass
	*/
	removeClass(className) {
		
		this.class.delete(className);
		
		//	TODO: remove class if component is rendered
		
	}
	
	/**
	* Remove css style from component
	*
	* @method removeStyle
	*/
	removeStyle(property) {
		
		this.style.delete(property);
		
		//	TODO: remove style if component is rendered
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {

		//	first check if component alredy exists in the DOM
		if (this.el) {
			this.refresh();
			return;
		}
		
		//	create component wrapper
		this.el = jQuery('<div id="' + this.id + '"></div>');
		
		//	TODO: border

		//	add css class
		if (this.class) { for (let cssClass of this.class) this.el.addClass(cssClass); }
		
		//	border
		if (this.border) this.el.addClass('border');
		
		//	if custom style, add to element
		this.applyStyle();
		
		if (parentEl)
			this.el.appendTo(parentEl);
		
		this.init();
		
	}
	
}