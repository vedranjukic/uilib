import { ComponentFocus } from '../componentFocus.js';
import { VendorButton } from '../vendor/button/button.js';

/**
 * Simple button component.
 *
 * @class Button
 * @extends ComponentFocus
 */
export class Button extends ComponentFocus {
    
	/**
	 * @class Button
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);

		this.text = '';
		
		this.initialize(params);

    }
	
	//
	//	
	//	Properties
	//
	//
	
	/**
	* Button text
	* 
	* @property text
	* @type {string}
	*/
	set text(newValue) {
		this._text = newValue;
		this.refresh();
	}
	get text() {
		return this._text;
	}
	
	//
	//	
	//	Methods
	//
	//
	
	refresh() {
		
		super.refresh();
		
		if (!this.el) return false;
		
		VendorButton.refresh(this);
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		//	don't inherit render from component ancestor so we don't spam DOM with extra div
		
		//	first check if component alredy exists in the DOM
		if (this.el) return;
		
		//	create component wrapper
		this.el = jQuery('<button id="' + this.id + '"></button>');

		//	add css class
		if (this.class) { for (let cssClass of this.class) this.el.addClass(cssClass); }
		
		//	if custom style, add to element
		this.applyStyle();
		
		if (parentEl)
			this.el.appendTo(parentEl);
		
		this.ref = VendorButton.create(this);
		
	}
	
	//
	//	
	//	Events
	//
	//
	
	//
	//	
	//	Internal
	//
	//
	
	initialize(params) {
	
		super.initialize(params);
		
		if (params.text) this.text = params.text;
		
	}
	
}