/**
 * Provides a registry of all Components (instances of Component or any subclass thereof) on a page so that they can be easily accessed by component id.
 *
 * @class ComponentManager
 */
export class ComponentManager {
    
	/**
	 * @class ComponentManager
	 * @constructor
	 */
	constructor(ui) {
		
		this.ui = ui;
		this.components = new Map();
		
		this._lastId = 100;	//	start from 100
		
    }
	
	/**
	 * Get component from ComponentManager.
	 *
	 * @method get
	 * @param {int} id Id of Component added to ComponentManager
	 */
	get(id) {
		
		return this.components.get(id);
		
	}
	
	/**
	 * Get component from ComponentManager using component name property.
	 *
	 * @method getByName
	 * @param {string} name Name of Component added to ComponentManager
	 */
	getByName(name) {
		
		//	TODO: better way to itterate set?! this does not seem right :)
		for (let component of this.components) {
			if (component[1].name == name) return component[1];
		}

		return false;
		
	}
	
	/**
	 * Registers an item to be managed
	 * id property will be assigned to Component 
	 *
	 * @method register
	 * @param {Component} component Instance of Component or any subclass thereof
	 */
	register(component) {
		
		component.id = 'c' + this._lastId++;
		
		this.components.set(component.id, component);
		
	}
	
	/**
	 * Unregisters an item by removing it from this manager
	 *
	 * @method unregister
	 * @param {Component} component Instance of Component or any subclass thereof
	 */
	unregister(id) {
		
		this.components.delete(id);
		
	}
	
}