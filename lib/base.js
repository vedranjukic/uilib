/**
 * Base class for all UI classes.
 * Only for internal use
 *
 * @class Base
 */
export class Base {
    
	/**
	* Get Component Class Name. Usefull for extended classes.
	*
	* @method getTypeName
	*/
	getTypeName() {
		
		var funcNameRegex = /function (.{1,})\(/;
		var results = (funcNameRegex).exec((this).constructor.toString());
		return (results && results.length > 1) ? results[1] : "";
		
	}
	
}