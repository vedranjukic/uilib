import { Component } from '../component.js';

/**
 * Base class for any Ext.Component that may contain other Components. 
 * Containers handle the basic behavior of containing items, namely adding, inserting and removing items.
 *
 * @class HtmlFrame
 * @extends Component
 */
export class HtmlFrame extends Component {
    
	/**
	 * @class HtmlFrame
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		this.html = '';
		
		this.initialize(params);
		
    }
	
	
	/**
	* Html code to be rendered inside component container
	* 
	* @property html
	* @type {string}
	*/
	set html(newValue) {
		this._html = newValue;
		this.refresh();
	}
	get html() {
		return this._html;
	}
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.html) this.html = params.html;
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		
		if (this.html) {
			
			this.el.html(this.html);
			
		}
		
	}
	
	refresh() {
		
		if (this.el && this.html) this.el.html(this.html);
		
	}
	
}