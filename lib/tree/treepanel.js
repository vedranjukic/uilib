import { Panel } from '../panel/panel.js';
import { VendorTreePanel } from '../vendor/tree/treepanel.js';

/**
 * The TreePanel provides tree-structured UI representation of tree-structured data.
 *
 * @class TreePanel
 * @extends Panel
 */
export class TreePanel extends Panel {
    
	/**
	 * @class TreePanel
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {

		super(params);
		
		this.initialize(params);
		
    }
	
	/**
	* Add node item to tree
	*
	* @method addItem
	* @param {Object} parent Parent node to add new node item to
	* @param {String} name Node item name
	*/
	addItem(params) {
		
		if (!params)
			throw new Error('Missing params');
		
		var node = {
			parent: params.parent,
			name: params.name
		};

		return VendorTreePanel.addItem({
			tree: this, 
			item: node
		});
		
	}
	
	/**
	* Collapse node item
	*
	* @method collapseItem
	* @param {Object} node Node item to collapse
	*/
	collapseItem(node) {
		
		if (!node)
			throw new Error('Missing node param');

		return VendorTreePanel.collapseItem({
			tree: this, 
			item: node
		});
		
	}
	
	/**
	* Expand node item
	*
	* @method expandItem
	* @param {Object} node Node item to expand
	*/
	expandItem(node) {
		
		if (!node)
			throw new Error('Missing node param');

		return VendorTreePanel.expandItem({
			tree: this, 
			item: node
		});
		
	}
	
	/**
	* On node item click event
	*
	* @event onItemClick
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	onItemClick(params) {
		
		if (!params)
			throw new Error('Missing params');
		
		if (!params.item)
			throw new Error('Missing item param');
		
		if (!params.cb)
			throw new Error('Missing cb param');

		return VendorTreePanel.onItemClick({
			tree: this, 
			item: params.item,
			cb: params.cb
		});
		
	}
	
	/**
	* On node item collapse event
	*
	* @event onItemCollapse
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	onItemCollapse(params) {
		
		if (!params)
			throw new Error('Missing params');
		
		if (!params.item)
			throw new Error('Missing item param');
		
		if (!params.cb)
			throw new Error('Missing cb param');

		return VendorTreePanel.onItemCollapse({
			tree: this, 
			item: params.item,
			cb: params.cb
		});
		
	}
	
	/**
	* On node item expand event
	*
	* @event onItemExpand
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	onItemExpand(params) {
		
		if (!params)
			throw new Error('Missing params');
		
		if (!params.item)
			throw new Error('Missing item param');
		
		if (!params.cb)
			throw new Error('Missing cb param');

		return VendorTreePanel.onItemExpand({
			tree: this, 
			item: params.item,
			cb: params.cb
		});
		
	}
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
	}
	
	refresh() {
		
		if (!super.refresh()) return false;
		
		super.refresh();
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		
		VendorTreePanel.create(this);
		
	}
	
}