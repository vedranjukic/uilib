import { Base } from '../base.js';

/**
 * The DataSet encapsulates a client side cache of data model objects.
 * DataSet also provide functions for sorting, filtering and querying the model instances contained within it.
 * Each change to data inside DataSet triggers update to all components hooked with it.
 *
 * @class DataSet
 */
export class DataSet extends Base {
    
	/**
	 * @class DataStore
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		this._hooks = new Set();
		
		/**
		* Data record list
		* Array of data objects (records) that represend dataset
		* 
		* @property data
		* @type {Array}
		*/
		this.data = [];
		
		/**
		* Fields configuration
		* Array of {name, type} objects, where "name" is field name and "type" is data type
		* 
		* @property fields
		* @type {string}
		*/	
		this.fields = [];
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		if (params.data) this.data = params.data;
		if (params.fields) this.fields = params.fields;
		
	}
	
	get data() {
		return this._data;
	}
	set data(newVal) {

		this._data = (Array.isArray(newVal))?newVal:[];
		
		//	notify all hooks of data change
		for (let component of this._hooks) component.onDataSetChange();
		
	}
	
	/**
	* Hook a component with a Dataset
	*
	* @method hook
	* @param {Component} component
	*/
	hook(component) {
		this._hooks.add(component);
	}
	
	/**
	* Unhook a component from a Dataset
	*
	* @method unhook
	* @param {Component} component
	*/
	unhook(component) {
		this._hooks.remove(component);
	}
	
}