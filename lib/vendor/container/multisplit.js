export class VendorMultiSplit {
    
	constructor(params) {
		
    }
	
	static splitVertical(params) {

		params.minSize = 100;
		
		var divOne = params.el;
		var divTwo = params.el.next();
		var parentHeight = divOne.parent().height();
		divTwo.height(parentHeight - divOne.height());

		$(params.el).resizable({
			autoHide: false,
			handles: 's',
			minHeight: 100,
			resize: function(e, ui) {
				var parentHeight = ui.element.parent().height();
				var remainingSpace = parentHeight - ui.element.outerHeight();

				if (remainingSpace < params.minSize) {
					ui.element.height((parentHeight - params.minSize) / parentHeight * 100 + "%");
					remainingSpace = params.minSize;
				}
				var divTwo = ui.element.next(),
					divTwoHeight = (remainingSpace - (divTwo.outerHeight() - divTwo.height())) / parentHeight * 100 + "%";
				divTwo.height(divTwoHeight);
			},
			stop: function(e, ui) {
				var parentHeight = ui.element.parent().height();
				ui.element.css({
					height: ui.element.height() / parentHeight * 100 + "%"
				});
			}
		});
		
	}
	
	static splitHorizontal(params) {

		params.minSize = 100;
		
		var divOne = params.el;
		var divTwo = params.el.next();
		var parentWidth = divOne.parent().width();
		divTwo.width(parentWidth - divOne.width());
		
		$(params.el).resizable({
			autoHide: false,
			handles: 'e',
			minWidth: 100,
			resize: function(e, ui) {
				var parentWidth = ui.element.parent().width();
				var remainingSpace = parentWidth - ui.element.outerWidth();

				if (remainingSpace < params.minSize) {
					ui.element.width((parentWidth - params.minSize) / parentWidth * 100 + "%");
					remainingSpace = params.minSize;
				}
				var divTwo = ui.element.next(),
					divTwoWidth = (remainingSpace - (divTwo.outerWidth() - divTwo.width())) / parentWidth * 100 + "%";
				divTwo.width(divTwoWidth);
			},
			stop: function(e, ui) {
				var parentWidth = ui.element.parent().width();
				ui.element.css({
					width: ui.element.width() / parentWidth * 100 + "%"
				});
			}
		});
		
	}
	
}