export class VendorLayoutAccordion {
    
	constructor(params) {
		
    }
	
	//
	//	@params {Layout} layout
	//
	static finalize(params) {
		
		params.parent.ref.accordion( "refresh" );
		
	}
	
	//
	//	@params {Layout} layout
	//
	static create(params) {

		for (let panel of params.panels) {
			$('<div class="group"><h3>' + panel + '</h3><div></div></div>').appendTo(params.parent.el);
		}

		if (params.sortable) {
			
			params.parent.ref = params.parent.el.accordion({
				header: "> div > h3",
				heightStyle: "fill"
			}).sortable({
				axis: "y",
				handle: "h3",
				stop: function( event, ui ) {
					// IE doesn't register the blur when sorting
					// so trigger focusout handlers to remove .ui-state-focus
					ui.item.children( "h3" ).triggerHandler( "focusout" );

					// Refresh accordion to handle new order
					$( this ).accordion( "refresh" );
				}
			});
			
		} else {
			
			params.parent.ref = params.parent.el.accordion({
				heightStyle: "fill"
			});
			
		}
		
	}
	
	//
	//	@params {Layout} layout
	//	@params {Component} component
	//
	static place(params) {
		
		params.component.addClass('layout-accordion');
		
		let panelEl = params.layout.parent.el.find('> div > div:eq(' + params.component.region + ')');
		params.component.render(panelEl);
		params.component.el.appendTo(panelEl);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static refresh(params) {
		
		//	check if layout parent number of items differs from current layout accordion panels
		if (params.panels.length != params.parent.ref.children('.group').children('h3').length) {
			
			//	first remove all existing component wrappers from accordion panels
			params.parent.ref.html('');
			
			//	recreate accordion structure
			this.create(params);
			
			//	(re)place all components inside accordion panels
			for (let component of params.parent.items) {
				this.place({
					layout: params,
					component: component
				});
			}
			
		}
		
		params.parent.ref.accordion( "refresh" );
		
	}
	
}