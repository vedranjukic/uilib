export class VendorLayoutTab {
    
	constructor(params) {
		
    }
	
	//
	//	@params {Layout} layout
	//
	static finalize(params) {
		
		params.parent.el.children('.tabs').tabs( "refresh" );
		
	}
	
	//
	//	@params {Layout} layout
	//
	static create(params) {
		
		let wrapper = $('<div class="tabs"></div>');
		
		let ulTabs = $('<ul></ul>');

		let i = 0;
		for (let panel of params.panels) {
			i++;
			$('<li><a href="#' + params.parent.id + '_tab_' + i + '">' + panel + '</a></li>').appendTo(ulTabs);
		}
		
		ulTabs.appendTo(wrapper);
		
		i = 0;
		for (let panel of params.panels) {
			i++;
			$('<div id="' + params.parent.id + '_tab_' + i + '" class="tab"></div>').appendTo(wrapper);
		}
		
		wrapper.appendTo(params.parent.el);
		
		wrapper.tabs();
		
		if (params.sortable) {
			
			wrapper.find( ".ui-tabs-nav" ).sortable({
				  axis: "x",
				  stop: function() {
					wrapper.tabs( "refresh" );
				  }
			});
			
		}
		
	}
	
	//
	//	@params {Layout} layout
	//	@params {Component} component
	//
	static place(params) {
		
		params.component.addClass('layout-tab');
		
		let panelEl = params.layout.parent.el.children('.tabs').children('div.tab:eq(' + params.component.region + ')');
		params.component.render(panelEl);
		params.component.el.appendTo(panelEl);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static refresh(params) {
		
		//	check if layout parent number of items differs from current layout accordion panels
		if (params.panels.length != params.parent.el.children('.tabs').children('div.tab').length) {
			
			//	first remove all existing component wrappers from accordion panels
			params.el.html('');
			
			//	recreate accordion structure
			this.create(params);
			
			//	(re)place all components inside accordion panels
			for (let component of params.parent.items) {
				this.place({
					layout: params,
					component: component
				});
			}
			
		}
		
		params.parent.el.children('.tabs').tabs( "refresh" );
		
	}
	
}