export class VendorLayoutHBox {
    
	constructor(params) {
		
    }
	
	//
	//	@params {Layout} layout
	//
	static finalize(params) {
		
		VendorLayoutHBox.recalc(params);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static create(params) {
		
	}
	
	//
	//	@params {Layout} layout
	//
	static recalc(params) {
		
		//	if config is provided use config values
		if (params.config && params.config.panels) {
			
			//	create columns and distribute width percentage
			for (let i=0; i<params.parent.items.length; i++) {

				let width = (params.config.panels[i].width % 1 === 0)?params.config.panels[i].width + 'px':params.config.panels[i].width;
				
				if (params.parent.items.el) params.parent.items.el.parent().css('width', width + 'px');
				
			}
			
			return;
		}
		
		//	first calculate total number of columns

		params.columns = 0;
		for (let component of params.parent.items) {
			if (component.flex > 0) {
				params.columns = params.columns + component.flex;
			} else {
				params.columns++;
			}
		}
		
		//	calc width percentage
		var percentage = 100 / params.columns;
		
		//	create columns and distribute width percentage
		for (let component of params.parent.items) {
			let flex = (component.flex > 0)?component.flex:1;
			if (component.el) component.el.parent().css('width', (percentage * flex) + '%');
		}
		
	}
	
	//
	//	@params {Layout} layout
	//	@params {Component} component
	//
	static place(params) {
		
		params.component.addClass('layout-hbox');
		
		var column = $('<div class="column"></div>');
		column.appendTo(params.layout.parent.el);
		params.layout.lastComponentColumn++;
		
		VendorLayoutHBox.recalc(params.layout);
		
		params.component.render(column);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static refresh(params) {

		VendorLayoutHBox.recalc(params);
		
	}
	
}