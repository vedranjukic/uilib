export class VendorLayoutPage {
    
	constructor(params) {
		
    }
	
	//
	//	@params {Layout} layout
	//
	static finalize(params) {
		
		//	params.parent.el.children('.tabs').tabs( "refresh" );
		
	}
	
	//
	//	@params {Layout} layout
	//
	static create(params) {
		
		let wrapper = $('<div class="pages"></div>');
		
		for (let panel of params.parent.items) {
			$('<div class="page" style="display: none;"></div>').appendTo(wrapper);
		}
		
		wrapper.appendTo(params.parent.el);
		
	}
	
	//
	//	@params {Layout} layout
	//	@params {Component} component
	//
	static place(params) {
		
		params.component.addClass('layout-page');
		
		let panelEl = params.layout.parent.el.children('.pages').children('div.page:eq(' + params.component.region + ')');
		params.component.render(panelEl);
		params.component.el.appendTo(panelEl);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static refresh(params) {
		
		//	check if layout parent number of items differs from current layout accordion panels
		if (params.parent.items.length != params.parent.el.children('.pages').children('div.page').length) {
			
			//	first remove all existing component wrappers from accordion panels
			params.el.html('');
			
			//	recreate accordion structure
			this.create(params);
			
			//	(re)place all components inside accordion panels
			for (let component of params.parent.items) {
				this.place({
					layout: params,
					component: component
				});
			}
			
		}
		
		params.parent.el.children('.pages').children('div.page').hide();
		params.parent.el.children('.pages').children('div.page:eq(' + params.parent.page + ')').show();
		
	}
	
}