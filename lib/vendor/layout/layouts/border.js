export class VendorLayoutBorder {
    
	constructor(params) {
		
    }
	
	static create(params) {
		
		var html = '';
		
		var options = {
			enableCursorHotkey:			false,
			spacing_open: 0,
			spacing_closed: 0, 
			closable:					false,	// pane can open & close
			resizable:					false,	// when open, pane can be resized 
			slidable:					false,	// when closed, pane can 'slide' open over other panes - closes on mouse-out
			livePaneResizing:			false
		};
		
		function onResize(cb, n, e, s, o, l) {
			
			this.refresh();
			
		}
		
		if (params.north) {
			html += '<div class="ui-layout-north"></div>';
			options.north = {};
			if (params.north.sizable) {
				options.north.resizable = true;
				options.north.closable  = true;
				options.north.spacing_open = 1;
				options.north.spacing_closed = 2;
				options.north.onresize_end = onResize.bind(params.parent);
				options.north.onclose_end = onResize.bind(params.parent);
				options.north.onclose = function() { /* $(window).resize(); */ };
			}
			if (params.north.size) options.north.size = params.north.size;
			if (params.north.closed) options.north.initClosed = true;
		}
		
		if (params.east) {
			
			html += '<div class="ui-layout-east"></div>';
			options.east = {};
			if (params.east.sizable) {
				options.east.resizable = true;
				options.east.closable  = true;
				options.east.spacing_open = 1;
				options.east.spacing_closed = 2;
				options.east.onresize_end =  onResize.bind(params.parent);
				options.east.onclose_end =  onResize.bind(params.parent);
				options.east.onclose = function() { /* $(window).resize(); */ };
			}
			if (params.east.size) options.east.size = params.east.size;
			if (params.east.closed) options.east.initClosed = true;
			
		}
		
		if (params.south) {
			
			html += '<div class="ui-layout-south"></div>';
			options.south = {};
			if (params.south.sizable) {
				options.south.resizable = true;
				options.south.closable  = true;
				options.south.spacing_open = 1;
				options.south.spacing_closed = 2;
				options.south.onresize_end =  onResize.bind(params.parent);
				options.south.onclose_end =  onResize.bind(params.parent);
				options.south.onclose = function() { /* $(window).resize(); */ };
				options.south.fxName = 'none';
			}
			if (params.south.size) options.south.size = params.south.size;
			if (params.south.closed) options.south.initClosed = true;
			
		}
		
		if (params.west) {
			html += '<div class="ui-layout-west"></div>';
			options.west = {};
			if (params.west.sizable) {
				options.west.resizable = true;
				options.west.closable  = true;
				options.west.spacing_open = 1;
				options.west.spacing_closed = 2;
				options.west.onresize_end =  onResize.bind(params.parent);
				options.west.onclose_end =  onResize.bind(params.parent);
				options.west.onclose = function() { /*	$(window).resize(); */ };
			}
			if (params.west.size) options.west.size = params.west.size;
			if (params.west.closed) options.west.initClosed = true;
		}
		
		html += '<div class="ui-layout-center"></div>';

		var el = $('<div class="layout-border-wrapper">' + html + '</div>');
		
		params.el.append(el);

		var layout = el.layout(options);
		
		layout.resizeAll();

		return layout;
		
	}
	
}