export class VendorLayoutVBox {
    
	constructor(params) {
		
    }
	
	//
	//	@params {Layout} layout
	//
	static finalize(params) {
		
		VendorLayoutVBox.recalc(params);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static create(params) {
		
	}
	
	//
	//	@params {Layout} layout
	//
	static recalc(params) {
		
		//	if config is provided use config values
		if (params.config && params.config.panels) {
			
			//	create columns and distribute height percentage
			for (let i=0; i<params.parent.items.length; i++) {

				let height = (params.config.panels[i].height % 1 === 0)?params.config.panels[i].height + 'px':params.config.panels[i].height;
				
				if (params.parent.items.el) params.parent.items.el.parent().css('height', height + 'px');
				
			}
			
			return;
		}
		
		//	first calculate total number of columns

		params.columns = 0;
		for (let component of params.parent.items) {
			if (component.flex > 0) {
				params.columns = params.columns + component.flex;
			} else {
				params.columns++;
			}
		}
		
		//	calc height percentage
		var percentage = 100 / params.columns;
		
		//	create columns and distribute height percentage
		for (let component of params.parent.items) {
			let flex = (component.flex > 0)?component.flex:1;
			if (component.el) component.el.parent().css('height', (percentage * flex) + '%');
		}
		
	}
	
	//
	//	@params {Layout} layout
	//	@params {Component} component
	//
	static place(params) {
		
		params.component.addClass('layout-vbox');
		
		var column = $('<div class="row"></div>');
		column.appendTo(params.layout.parent.el);
		params.layout.lastComponentColumn++;
		
		VendorLayoutVBox.recalc(params.layout);
		
		params.component.render(column);
		
	}
	
	//
	//	@params {Layout} layout
	//
	static refresh(params) {

		VendorLayoutVBox.recalc(params);
		
	}
	
}