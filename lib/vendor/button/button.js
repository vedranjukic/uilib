export class VendorButton {
    
	constructor(params) {
		
    }
	
	static create(params) {
		
		var config = {
			
			disabled: params.disabled,
			icons: params.icons,
			label: params.text,
			text: (params.text)
			
		};
		
		var ref = params.el.button(config);
		
		ref.on('click', params.onClick);
		
		return ref;
		
	}
	
	static refresh(params) {
		
		params.el.find('span').html(params.text);
		
	}
	
}