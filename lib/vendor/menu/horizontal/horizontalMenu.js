export class VendorHorizontalMenu {
    
	constructor(params) {
		
    }
	
	static create(params) {

		var config = {
			openClick: true, // Submenu opens by clicking or hovering.
			ulWidth: (params.subMenuWidth)?params.subMenuWidth:'auto', //	Fix the the sub-menus widths.
			absoluteTop: 30, //	The top pos (absolute) in pixel of the sub-menu compared to the parent.
			absoluteLeft: 0, //	The left pos (absolute) in pixel of the sub-menu compared to the parent.
			effects : {
				effectSpeedOpen: 150, //	In ms, slideDown/fadeIn speed of the sub-menus.
				effectSpeedClose: 150, //	In ms, slideUp/fadeOut speed of the sub-menus.
				effectTypeOpen: 'show', //	Can be set to 'slide', 'fade' or '' (empty)
				effectTypeClose: 'hide', //	Can be set to 'slide', 'fade' or 'hide' (empty)
				effectOpen: 'linear', //	All jQuery UI effects allowed
				effectClose: 'linear', //	All jQuery UI effects allowed
			},
			TimeBeforeOpening: 100, // In ms, waiting time before sub-menus slideDown/fadeIn effects.
			TimeBeforeClosing: 100, //	In ms, waiting time before sub-menus slideUp/fadeOut effects.
			animatedText: false, //	If true, animations on the hover (see the next option paddingLeft).
			paddingLeft: 7 //	In pixel, the padding-left animation on the hover.
		};

		var ref = params.el.children('ul').jMenu(config);
		
		return ref;
		
	}
	
}