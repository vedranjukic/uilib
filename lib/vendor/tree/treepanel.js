export class VendorTreePanel {
    
	constructor(params) {
		
    }
	
	/*
	* Add node item to tree
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	*/
	static addItem(params) {
		
		var tree = params.tree;
		var item = params.item;
		
		var node_id = tree.el.jstree().create_node(item.parent, item.name);
		
		return tree.el.jstree().get_node(node_id);
		
	}
	
	/*
	* Collapse node item
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	*/
	static collapseItem(params) {
		
		var tree = params.tree;
		var item = params.item;

		return tree.el.jstree().close_node(item);
		
	}
	
	/*
	* Expand node item
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	*/
	static expandItem(params) {
		
		var tree = params.tree;
		var item = params.item;

		return tree.el.jstree().open_node(item);
		
	}
	
	/*
	* On node item click event
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	static onItemClick(params) {
		
		var tree = params.tree;
		var item = params.item;
		var cb = params.cb;
		
		//
		//	initialize event listener callback
		//
		//	NOTE: jstree uses global event listener for events
		//	to make it possible to listen event for each node item
		//	we need to store node item callbacks and make custom
		//	event handler to call specific node item callback from
		//	global jstree event listener
		//  we'll create vendorTreePanel object and bind it to the tree
		//	it will be used as a namespace for onItemClick pointer and
		//	onItemClickStore callback map
		//
		//	NOTE: event listener is binded to jstree in create function
		//
		
		if (!tree.vendorTreePanel)
			tree.vendorTreePanel = {};
		
		if (!tree.vendorTreePanel.onItemClickStore)
			tree.vendorTreePanel.onItemClickStore = new Map();
		
		if (!tree.vendorTreePanel.onItemClick)
			tree.vendorTreePanel.onItemClick = function(e, data) {

				var fn = tree.vendorTreePanel.onItemClickStore.get(data.node.id);
				if (typeof fn === 'function') fn(data.node);
				
			};
		
		tree.vendorTreePanel.onItemClickStore.set(item.id, cb);
		
	}
	
	/*
	* On node item expand event
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	static onItemExpand(params) {
		
		var tree = params.tree;
		var item = params.item;
		var cb = params.cb;
		
		//
		//	initialize event listener callback
		//
		//	NOTE: jstree uses global event listener for events
		//	to make it possible to listen event for each node item
		//	we need to store node item callbacks and make custom
		//	event handler to call specific node item callback from
		//	global jstree event listener
		//  we'll create vendorTreePanel object and bind it to the tree
		//	it will be used as a namespace for onItemExpand pointer and
		//	onItemExpandStore callback map
		//
		//	NOTE: event listener is binded to jstree in create function
		//
		
		if (!tree.vendorTreePanel)
			tree.vendorTreePanel = {};
		
		if (!tree.vendorTreePanel.onItemExpandStore)
			tree.vendorTreePanel.onItemExpandStore = new Map();
		
		if (!tree.vendorTreePanel.onItemExpand)
			tree.vendorTreePanel.onItemExpand = function(e, data) {
			
				var fn = tree.vendorTreePanel.onItemExpandStore.get(data.node.id);
				if (typeof fn === 'function') fn(data.node);
				
			};
		
		tree.vendorTreePanel.onItemExpandStore.set(item.id, cb);
		
	}
	
	/*
	* On node item collapse event
	*
	* @param {TreePanel} tree TreePanel component
	* @param {Object} item Node item
	* @param {Function} cb Callback function
	*/
	static onItemCollapse(params) {
		
		var tree = params.tree;
		var item = params.item;
		var cb = params.cb;
		
		//
		//	initialize event listener callback
		//
		//	NOTE: jstree uses global event listener for events
		//	to make it possible to listen event for each node item
		//	we need to store node item callbacks and make custom
		//	event handler to call specific node item callback from
		//	global jstree event listener
		//  we'll create vendorTreePanel object and bind it to the tree
		//	it will be used as a namespace for onItemCollapse pointer and
		//	onItemCollapseStore callback map
		//
		//	NOTE: event listener is binded to jstree in create function
		//
		
		if (!tree.vendorTreePanel)
			tree.vendorTreePanel = {};
		
		if (!tree.vendorTreePanel.onItemCollapseStore)
			tree.vendorTreePanel.onItemCollapseStore = new Map();
		
		if (!tree.vendorTreePanel.onItemCollapse)
			tree.vendorTreePanel.onItemCollapse = function(e, data) {
			
				var fn = tree.vendorTreePanel.onItemCollapseStore.get(data.node.id);
				if (typeof fn === 'function') fn(data.node);
				
			};
		
		tree.vendorTreePanel.onItemCollapseStore.set(item.id, cb);
		
	}
	
	static create(params) {
		
		var tree = params.el
			.on('activate_node.jstree', function (e, data) {
				this.vendorTreePanel.onItemClick(e, data);
			}.bind(params))
			.on('open_node.jstree', function (e, data) {
				this.vendorTreePanel.onItemExpand(e, data);
			}.bind(params))
			.on('close_node.jstree', function (e, data) {
				this.vendorTreePanel.onItemCollapse(e, data);
			}.bind(params))
			.jstree({
				core: {
					'check_callback' : true
				}
			});
		
	}
	
}