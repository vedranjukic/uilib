export class Form {
    
	static init(params) {
		
		params.elForm.ajaxForm({
			beforeSubmit: params.onSubmit.bind(this)
		});
		
	}
	
}