export class FormValidator {
    
	static init(params) {
		
		var rules = {};
		var messages = {};
		
		for (let component of params.getFieldComponents()) {
			
			if (!component.fieldValidation || !component.fieldName) continue;
			
			rules[component.fieldName] = component.fieldValidation;
			
			delete rules[component.fieldName].messages;
			
		}
		
		for (let component of params.getFieldComponents()) {
			
			if (!component.fieldValidation || !component.fieldName || !component.fieldValidation.messages) continue;
			
			messages[component.fieldName] = component.fieldValidation.messages;
			
		}
		
		var validate = {
			
			rules: rules,
			messages: messages
			
		};

		params.elForm.validate(validate);
		
	}
	
}