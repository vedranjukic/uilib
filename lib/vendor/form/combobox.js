export class VendorComboBox {
    
	constructor(params) {
		
    }
	
	static create(params) {
		
		var config = {
			
		};

		if (params.ref) params.ref.selectmenu( "destroy" );
		
		var ref = params.el.selectmenu(config);
		
		ref.on('change', params.onChange);
		ref.on('click', params.onClick);
		ref.on('close', params.onClose);
		ref.on('disable', params.onDisable);
		ref.on('enable', params.onEnable);
		ref.on('focus', params.onFocus);
		ref.on('open', params.onOpen);
		ref.on('select', params.onSelect);
		
		return ref;
		
	}
	
}