export class VendorWindow {
    
	constructor(params) {
		
    }
	
	static close(params) {
		
		return params.ref.dialog( "close" );
		
	}
	
	static isOpen(params) {
		
		return params.ref.dialog( "isOpen" );
		
	}
	
	static moveToTop(params) {
		
		return params.ref.dialog( "moveToTop" );
		
	}
	
	static open(params) {
		
		return params.ref.dialog( "open" );
		
	}
	
	static create(params) {
		
		var buttons = {};
		for (let button of params.buttons) {
			buttons[button.text] = button.onClick;
		}
		
		var config = {
			autoOpen: false,
			
			buttons: buttons,
			
			closeOnEscape: params.closeOnEscape,
			maxHeight: params.maxHeight,
			maxWidth: params.maxWidth,
			minHeight: params.minHeight,
			minWidth: params.minWidth,
			modal: params.modal,
			resizable: params.resizable,
			
			width: (params.width)?params.width:300,
			height: (params.height)?params.height:200,
			beforeClose: params.onBeforeClose,
			close: params.onClose,
			drag: params.onDrag,
			dragStart: params.onDragStart,
			dragStop: params.onDragStop,
			focus: params.onFocus,
			open: params.onOpen,
			resize: params.onResize,
			resizeStart: params.onResizeStart,
			resizeStop: params.onResizeStop
		};
		
		return params.el.dialog(config);
		
	}
	
}