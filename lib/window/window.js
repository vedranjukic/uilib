import { Panel } from '../panel/panel.js';
import { VendorWindow } from '../vendor/window/window.js';

/**
 * A specialized panel intended for use as an application window.
 *
 * @class Container
 * @extends Panel
 */
export class Window extends Panel {
    
	/**
	 * @class Window
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		this.parent = 'body';
		
		/**
		* Array of buttons
		* 
		* @property buttons
		* @type {Array}
		*/
		this.buttons = [];
		
		/**
		* Specifies whether the dialog should close when it has focus and the user presses the escape (ESC) key
		* 
		* @property closeOnEscape
		* @type {bool}
		* @default false
		*/
		this.closeOnEscape = false;
		
		/**
		* The maximum height to which the dialog can be resized, in pixels
		* 
		* @property maxHeight
		* @type {int}
		*/
		this.maxHeight = false;
		
		/**
		* The maximum width to which the dialog can be resized, in pixels
		* 
		* @property maxWidth
		* @type {int}
		*/
		this.maxWidth = false;
		
		/**
		* The minimum height to which the dialog can be resized, in pixels
		* 
		* @property minHeight
		* @type {int}
		*/
		this.minHeight = false;
		
		/**
		* The minimum width to which the dialog can be resized, in pixels
		* 
		* @property minWidth
		* @type {int}
		*/
		this.minWidth = false;
		
		/**
		* If set to true, the dialog will have modal behavior; other items on the page will be disabled, i.e., cannot be interacted with
		* Modal dialogs create an overlay below the dialog but above other page elements
		* 
		* @property modal
		* @type {bool}
		* @default false
		*/
		this.modal = false;
		
		/**
		* If set to true, the dialog will be resizable
		* 
		* @property resizable
		* @type {bool}
		* @default false
		*/
		this.resizable = false;
		
		/**
		* Before window close event handler
		*
		* @event onBeforeClose
		*/
		this.onBeforeClose = function(){};
		
		/**
		* After window close event handler
		*
		* @event onClose
		*/
		this.onClose = function(){};
		
		/**
		* On window drag event handler
		*
		* @event onDrag
		*/
		this.onDrag = function(){};
		
		/**
		* On window drag start event handler
		*
		* @event onDragStart
		*/
		this.onDragStart = function(){};
		
		/**
		* On window drag stop event handler
		*
		* @event onDragStop
		*/
		this.onDragStop = function(){};
		
		/**
		* On window focus event handler
		*
		* @event onFocus
		*/
		this.onFocus = function(){};
		
		/**
		* On window open event handler
		*
		* @event onOpen
		*/
		this.onOpen = function(){};
		
		/**
		* On window resize event handler
		*
		* @event onResize
		*/
		this.onResize = function(){};
		
		/**
		* On window resize start event handler
		*
		* @event onResizeStart
		*/
		this.onResizeStart = function(){};
		
		/**
		* On window resize stop event handler
		*
		* @event onResizeStop
		*/
		this.onResizeStop = function(){};
		
		this.initialize(params);
		
		this.render();
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {

		super.initialize(params);
		
		if (Array.isArray(params.buttons)) this.buttons = params.buttons;
		if (params.closeOnEscape) this.closeOnEscape = params.closeOnEscape;
		if (params.maxHeight) this.maxHeight = params.maxHeight;
		if (params.maxWidth) this.maxWidth = params.maxWidth;
		if (params.minHeight) this.minHeight = params.minHeight;
		if (params.minWidth) this.minWidth = params.minWidth;
		if (params.modal) this.modal = params.modal;
		if (params.resizable) this.resizable = params.resizable;
		
		if (params.onBeforeClose && typeof params.onBeforeClose != 'function') throw new Error("Not a valid function");
		if (params.onBeforeClose) this.onBeforeClose = params.onClick;
		
		if (params.onClose && typeof params.onClose != 'function') throw new Error("Not a valid function");
		if (params.onClose) this.onClose = params.onClose;
		
		if (params.onDrag && typeof params.onDrag != 'function') throw new Error("Not a valid function");
		if (params.onDrag) this.onDrag = params.onDrag;
		
		if (params.onDragStart && typeof params.onDragStart != 'function') throw new Error("Not a valid function");
		if (params.onDragStart) this.onDragStart = params.onDragStart;
		
		if (params.onDragStop && typeof params.onDragStop != 'function') throw new Error("Not a valid function");
		if (params.onDragStop) this.onDragStop = params.onDragStop;
		
		if (params.onFocus && typeof params.onFocus != 'function') throw new Error("Not a valid function");
		if (params.onFocus) this.onFocus = params.onFocus;
		
		if (params.onOpen && typeof params.onOpen != 'function') throw new Error("Not a valid function");
		if (params.onOpen) this.onOpen = params.onOpen;
		
		if (params.onResize && typeof params.onResize != 'function') throw new Error("Not a valid function");
		if (params.onResize) this.onOpen = params.onResize;
		
		if (params.onResizeStart && typeof params.onResizeStart != 'function') throw new Error("Not a valid function");
		if (params.onResizeStart) this.onOpen = params.onResizeStart;
		
		if (params.onResizeStop && typeof params.onResizeStop != 'function') throw new Error("Not a valid function");
		if (params.onResizeStop) this.onOpen = params.onResizeStop;
		
	}
	
	/**
	* Close window
	*
	* @method close
	*/
	close() {
	
		return VendorWindow.close(this);
		
	}
	
	/**
	* Whether the window is currently open
	*
	* @method isOpen
	*/
	isOpen() {
		
		return VendorWindow.isOpen(this);
		
	}

	/**
	* Moves the window to the top of the window stack
	*
	* @method moveToTop
	*/
	moveToTop() {
		
		return VendorWindow.moveToTop(this);
		
	}
	
	/**
	* Opens the window
	*
	* @method open
	*/
	open() {
		
		return VendorWindow.open(this);
		
	}
	
	refresh() {
		
		if (!this.el || !this.ref) return false; 
		
		this.ref.dialog( "option", "height", this.height );
		this.ref.dialog( "option", "title", this.title );
		this.ref.dialog( "option", "width", this.width );
		
	}
	
	/**
	 * Render the base object and create window object reference
	 * 
	 * @method render
	 */
	render() {
		
		super.render();
		
		this.ref = VendorWindow.create(this);
		
	}
	
}