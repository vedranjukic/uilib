import { Panel } from './panel.js';

/**
 * A basic tab container. TabPanels can be used exactly like a standard Panel for layout purposes, but also have special support for containing child Components (tabs) that are managed using a Tab layout manager, and displayed as separate tabs.
 *
 * @class TabPanel
 * @extends Container
 */
export class TabPanel extends Panel {
    
	/**
	 * @class TabPanel
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);

	}
	
	refresh() {
		
		if (!super.refresh()) return false;

	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);

	}
	
}