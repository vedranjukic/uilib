import { Container } from '../container/container.js';

/**
 * Panel is a container that has specific functionality and structural components that make it the perfect building block for application-oriented user interfaces.
 * Panels are, by virtue of their inheritance from Container, capable of being configured with a layout, and containing child Components.
 *
 * @class Panel
 * @extends Container
 */
export class Panel extends Container {
    
	/**
	 * @class Panel
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Panel title
		* 
		* @property title
		* @type {string}
		*/
		this.title = false;
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.title) this.title = params.title;
		
	}
	
	get title() {
		return this._title;
	}
	set title(newValue) {
		this._title = newValue;
		this.refresh();
	}
	
	refresh() {
		
		if (!super.refresh()) return false;

		if (this.title) this.el.attr('title', this.title);
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		if (this.title) this.el.attr('title', this.title);
		
	}
	
}