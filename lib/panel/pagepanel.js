import { Panel } from './panel.js';

/**
 * PagePanel can be used exactly like a standard Panel for layout purposes, but also have special support for containing child Components (pages) that are managed using a Page layout manager.
 * One page is shown at a time, using page propery.
 *
 * @class PagePanel
 * @extends Panel
 */
export class PagePanel extends Panel {
    
	/**
	 * @class PagePanel
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Active Page
		* 
		* @property page
		* @type {int}
		*/
		this.page = 0;
		
		/**
		* Layout type for TabPanel
		* Should be left default
		* 
		* @property layout
		* @type {string}
		* @default 'tab'
		*/
		//	params.layout = 'tab';
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.title) this.title = params.title;
		
	}
	
	get page() {
		return this._page;
	}
	set page(newValue) {
		this._page = newValue;
		this.refresh();
	}
	
	refresh() {
		
		if (!super.refresh()) return false;

	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);

		this.refresh();
		
	}
	
}