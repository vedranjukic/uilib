import { Layout } from '../../layout/layout.js';

/**
 * Column Layout class
 *
 * A layout that arranges items horizontally across a Container. This layout optionally divides available horizontal space between child items containing a numeric flex configuration.
 * Component columnWidth property is always evaluated as a percentage, and must be a decimal value greater than 0 and less than 1 (e.g., .25).
 *
 * @class LayoutColumn
 * @extends Layout
 */
export class LayoutColumn extends Layout {
    
	/**
	 * @class LayoutColumn
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
		/**
		* Number of columns
		* 
		* @property columns
		* @type {int}
		* @default 0
		*/
		this.columns = 0;
		
		//	internal
		this.lastComponentColumn = 0;
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
		super.init();
		
		this.parent.el.addClass('hbox');
		
		//	first calculate total number of columns

		for (let component of this.parent.items) {
			this.columns++;
		}
		
		//	create columns and distribute width percentage
		for (let component of this.parent.items) {
			if (component.columnWidth < 0 || component.columnWidth > 1) 
				throw new Error("Invalid columnWidth property");
			$('<div class="column" style="width: ' + (100 * component.columnWidth) + '%"></div>').appendTo(this.parent.el);
		}
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	* @param {int} index Zero based index where the component is added in the layout
	*/
	place(params) {
		
		var component = params;
		var index = -1;
		
		if (params.component) component = params.component;
		if (params.index) index = params.index;
		
		component.addClass('layout-hbox');
		component.render(this.parent.el.children('div:eq(' + this.lastComponentColumn + ')'));
		
		this.lastComponentColumn++;
		
	}
	
}