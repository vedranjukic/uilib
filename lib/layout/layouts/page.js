import { Layout } from '../../layout/layout.js';
import { VendorLayoutPage } from '../../vendor/layout/layouts/page.js';

/**
 * Page Layout class
 *
 * @class LayoutPage
 * @extends Layout
 */
export class LayoutPage extends Layout {
    
	/**
	 * @class LayoutPage
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);

		/**
		* Layout panels
		* 
		* @property panels
		* @type {Array}
		*/
		this.panels = (Array.isArray(params.config.panels))?params.config.panels:[];
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {

		super.init();
		
		VendorLayoutPage.create(this);
		
	}
	
	/**
	* Refresh layout container. Some layouts need to perform additional operations on parent container resize.
	*
	* @method refresh
	*/
	refresh() {
		
		VendorLayoutPage.refresh(this);
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;
		
		VendorLayoutPage.place({
			layout: this,
			component: component
		});
		
	}
	
	/**
	* Called after all child components are placed inside layout container to perfrom additional tasks if needed to display layout properly
	*
	* @method finalize
	*/
	finalize() {
		
		super.finalize();
		
		VendorLayoutPage.finalize(this);
		
	}
	
}