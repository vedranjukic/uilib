import { Layout } from '../../layout/layout.js';
import { VendorLayoutHBox } from '../../vendor/layout/layouts/hbox.js';

/**
 * Horizontal Box Layout class
 *
 * A layout that arranges items horizontally across a Container. This layout optionally divides available horizontal space between child items containing a numeric flex configuration.
 *
 * @class LayoutHBox
 * @extends Layout
 */
export class LayoutHBox extends Layout {
    
	/**
	 * @class LayoutHBox
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
		/**
		* Number of columns
		* 
		* @property columns
		* @type {int}
		* @default 0
		*/
		this.columns = 0;
		
		this.lastComponentColumn = 0;
		
		this.config = (params.config)?params.config:false;
		
    }
	
	finalize() {
		
		VendorLayoutHBox.finalize(this);
		
	}
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
		super.init();
		
		this.parent.el.addClass('hbox');
		
		VendorLayoutHBox.create(this);
		
	}

	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	* @param {int} index Zero based index where the component is added in the layout
	*/
	place(params) {
		
		var component = params;
		var index = -1;
		
		if (params.component) component = params.component;
		if (params.index) index = params.index;
		
		VendorLayoutHBox.place({
			layout: this,
			component: component,
			index: index
		});
		
	}
	
	refresh() {
		
		VendorLayoutHBox.refresh(this);
		
	}
	
}