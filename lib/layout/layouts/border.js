import { Layout } from '../../layout/layout.js';
import { VendorLayoutBorder } from '../../vendor/layout/layouts/border.js';

/**
 * Border Layout class
 *
 * @class LayoutBorder
 * @extends Layout
 */
export class LayoutBorder extends Layout {
    
	/**
	 * @class LayoutBorder
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
		/**
		* Layout config
		* If not defined, will instanciate with default config
		*
		* Example:
		*
		* {
		*    north: {
		*      size: 100,
		*      sizable: true,
		*      closed: true,
		*      onResize: function() {...}
		*    },
		*    east: {
		*      size: '20%',
		*    },
		*    south: {...},
		*    west: {...}
		*  }
		*
		* 
		* @property config
		* @type {Object}
		*/
		this.config = (params.config)?params.config:this._defaultConfig();
		
    }
	
	_defaultConfig() {
		return {
			north: {
				size: 100
			},
			east: {
				size: '20%'
			},
			south: {
				size: 200,
				sizable: true
			},
			west: {
				size: '20%',
				sizable: true
			}
		};
	}
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {

		super.init();
		
		this.config.el = this.parent.el;
		this.config.parent = this.parent;
		
		VendorLayoutBorder.create(this.config);
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;
		
		component.addClass('layout-border');
		
		switch (component.region) {
			case 'north':
				component.render(this.parent.el.find('.ui-layout-north'));
				break;
			case 'east':
				component.render(this.parent.el.find('.ui-layout-east'));
				break;
			case 'south':
				component.render(this.parent.el.find('.ui-layout-south'));
				break;
			case 'west':
				component.render(this.parent.el.find('.ui-layout-west'));
				break;
			default:
				component.render(this.parent.el.find('.ui-layout-center'));
				break;
		}
		
	}
	
}