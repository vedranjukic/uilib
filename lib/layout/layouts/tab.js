import { Layout } from '../../layout/layout.js';
import { VendorLayoutTab } from '../../vendor/layout/layouts/tab.js';

/**
 * Tabs Layout class
 *
 * @class LayoutTab
 * @extends Layout
 */
export class LayoutTab extends Layout {
    
	/**
	 * @class LayoutTabs
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);

		/**
		* Layout panels
		* 
		* @property panels
		* @type {Array}
		*/
		this.panels = (Array.isArray(params.config.panels))?params.config.panels:[];
		this.sortable = params.config.sortable;
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {

		super.init();
		
		VendorLayoutTab.create(this);
		
	}
	
	/**
	* Refresh layout container. Some layouts need to perform additional operations on parent container resize.
	*
	* @method refresh
	*/
	refresh() {
		
		VendorLayoutTab.refresh(this);
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;
		
		VendorLayoutTab.place({
			layout: this,
			component: component
		});
		
	}
	
	/**
	* Called after all child components are placed inside layout container to perfrom additional tasks if needed to display layout properly
	*
	* @method finalize
	*/
	finalize() {
		
		super.finalize();
		
		VendorLayoutTab.finalize(this);
		
	}
	
}