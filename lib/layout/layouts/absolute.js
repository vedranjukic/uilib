import { Layout } from '../../layout/layout.js';

/**
 * The LayoutAuto is the default layout manager when no layout is configured into a Container. LayoutAuto provides only a passthrough of any layout calls to any child containers.
 *
 * @class LayoutAuto
 * @extends Layout
 */
export class LayoutAbsolute extends Layout {
    
	/**
	 * @class LayoutAuto
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
    }

	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;

		component.addClass('layout-absolute');
		component.addStyle('left', component.left);
		component.addStyle('top', component.top);
		component.render(this.parent.el);
		
	}
	
}