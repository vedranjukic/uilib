import { Layout } from '../../layout/layout.js';

/**
 * Fit Layout class
 *
 * @class LayoutFit
 * @extends Layout
 */
export class LayoutFit extends Layout {
    
	/**
	 * @class LayoutFit
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;

		component.addClass('layout-fit');
		component.render(this.parent.el);
		
	}
	
}