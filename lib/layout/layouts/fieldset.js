import { Layout } from '../../layout/layout.js';

/**
 * FieldSet Layout class
 * Layout for FieldSet Panel.
 *
 * @class LayoutFieldSet
 * @extends Layout
 */
export class LayoutFieldSet extends Layout {
    
	/**
	 * @class LayoutFieldSet
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
        
		super(params);
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
		super.init();

		//	create fieldset wrapper
		let el = $( '<fieldset></fieldset>' );
		
		if (this.parent.title) $( '<legend>' + this.parent.title + '</legend>' ).appendTo(el);
		
		el.appendTo(this.parent.el);		
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(component) {
		
		var component = component;
		if (component.component) component = component.component;
		
		component.render(this.parent.el.children('fieldset'));
		
	}
	
}