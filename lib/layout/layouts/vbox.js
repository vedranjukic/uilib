import { Layout } from '../../layout/layout.js';
import { VendorLayoutVBox } from '../../vendor/layout/layouts/vbox.js';

/**
 * Vertical Box Layout class
 *
 * A layout that arranges items vertically down a Container. This layout optionally divides available vertical space between child items containing a numeric flex configuration.
 *
 * @class LayoutVBox
 * @extends Layout
 */
export class LayoutVBox extends Layout {
    
	/**
	 * @class LayoutVBox
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
   
		super(params);
		
		/**
		* Number of rows
		* 
		* @property rows
		* @type {int}
		* @default 0
		*/
		this.rows = 0;
		
		this.lastComponentRow = 0;
		
		this.config = (params.config)?params.config:false;
		
    }
	
	finalize() {
		
		VendorLayoutVBox.finalize(this);
		
	}
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
		super.init();
		
		this.parent.el.addClass('vbox');
		
		VendorLayoutVBox.create(this);
		
	}
	
	/**
	* Place child component inside this layout
	*
	* @method place
	* @param {Component} component Child Component ancestor object
	*/
	place(params) {
		
		var component = params;
		var index = -1;
		
		if (params.component) component = params.component;
		if (params.index) index = params.index;
		
		VendorLayoutVBox.place({
			layout: this,
			component: component,
			index: index
		});
		
	}
	
	refresh() {
		
		VendorLayoutVBox.refresh(this);
		
	}
	
}