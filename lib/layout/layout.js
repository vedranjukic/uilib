/**
 * Base Layout class for all UI components.
 *
 * @class Layout
 */
export class Layout {
    
	/**
	 * @class Layout
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {

		if (!params) params = {};
		
		if (!params.parent)
			throw new Error("Parent not defined");
		
		/**
		* Parent component
		* 
		* @property parent
		* @type {Object}
		*/
		this.parent = params.parent;
		
    }
	
	/**
	* Intialize layout. Creates initial layout structure.
	*
	* @method initalize
	*/
	init() {
		
	}
	
	/**
	* Refresh layout container. Some layouts need to perform additional operations on parent container resize.
	*
	* @method refresh
	*/
	refresh() {
		
	}
	
	/**
	* Called after all child components are placed inside layout container to perfrom additional tasks if needed to display layout properly
	*
	* @method finalize
	*/
	finalize() {
		
	}
	
}