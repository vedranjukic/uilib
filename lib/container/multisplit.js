import { Container } from './container.js';
import { VendorMultiSplit } from '../vendor/container/multisplit.js';

/**
 * TODO...
 *
 * @class Multisplit
 * @extends Container
 */
export class Multisplit extends Container {
    
	/**
	 * @class Multisplit
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		//	internal - single dimension list of panels, based on creation index
		this._panelList = [];
		
		this.activePanel = 0;
		
		this.panels = [];

		/*
		this.panels = [
			{
				type: "vertical",
				size: "300",
				panels: [
					{
						type: "horizontal",
						size: "300",
						panels: [
							{
								type: "vertical",
								size: "100"
							},
							{
								type: "horizontal",
								size: "50"
							}
						]
					}
				]
			}	
		];
		*/
		
		this.initialize(params);
		
    }
	
	/**
	* @class Multisplit
	* Activate panel
	*
	* @param {Int} index Zero-based Panel index to activate
	*/
	activatePanel(index) {
		
		let panel = this.getPanel(index);
		
		if (!panel) return false;
		
		this.el.find('.ms-panel').removeClass('active');
		panel.addClass('active');
		
	}
	
	/**
	* @class Multisplit
	* Create new tab inside panel
	*
	* @param {Int} index Panel element using zero-based index to create new tab into
	* @param {Panel} component Panel object (or ancestor class) to assign to new tab. Title property of component will be used for tab title.
	*/
	createTab(params) {
		
		var panel = this.getPanel(params.index);
		
		var $ul = panel.children('ul');

		//	NOTE: something more eloquent?
		var tabId = 'mstab-' + Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1);
		
		var $li = jQuery('<li><a href="#' + tabId + '">Test</a></li>');
		$li.appendTo($ul);
		var $div = jQuery('<div id="' + tabId + '" class="mstab">TabID: ' + tabId + '</div>');
		$div.appendTo(panel);
		
		panel.tabs('refresh');
		
		//	find total tabs
		var panelTabIndex = $ul.children('li').length - 1;
				
		//	activate dropped tab in dest tab panel
		panel.tabs( "option", "active", panelTabIndex );
		
	}
	
	/**
	* @class Multisplit
	* Get panel
	*
	* @param {Int} index Return panel element using zero-based index
	*/
	getPanel(index) {
		
		if (!this._panelList[index]) return false;
		
		return this._panelList[index];
		
	}
	
	/*
	*
	*	Initialize tab panel [Private]
	*
	*	Initializes panel element to serve as tab container
	*	Handles tab drag&drop event between panels
	*
	*/
	initializeTabPanel(panel) {
		
		var $ul = jQuery('<ul>');
		$ul.appendTo(panel);
		panel.tabs();
		panel.find( ".ui-tabs-nav" ).sortable();
		panel.droppable({
			activeClass: "ui-state-highlight",
			drop: function (event, ui) {

				var $ul = this.children('ul');

				var $tab = ui.draggable;

				//	find panel element by using tabs href - both must match because of jquery-ui tabs plugin rules
				var $panel = jQuery($tab.children('a').attr('href'));
				if (!$panel.length) 
					throw new Error("Can not find panel container");

				//	create pointer to source panel wrapper
				var $panelSrc = $panel.closest('.ms-panel-content');
				//	get tab panel index
				var panelSrcTabIndex = $panelSrc.tabs( "option", "active" );
				panelSrcTabIndex--;
				if (panelSrcTabIndex < 0) 
					panelSrcTabIndex = 0;

				//	destroy existing tab
				$tab.remove();

				var $li = jQuery('<li><a href="#' + $tab.attr('aria-controls') + '">Test</a></li>');
				$li.appendTo($ul);

				$panel.appendTo(this);

				$panelSrc.tabs("refresh");
				$panelSrc.tabs( "option", "active", panelSrcTabIndex );

				this.tabs("refresh");
				
				//	find total tabs
				var panelTabIndex = $ul.children('li').length - 1;
				
				//	activate dropped tab in dest tab panel
				this.tabs( "option", "active", panelTabIndex );
				

			}.bind(panel)
		});
		
	}
	
	/**
	* @class Multisplit
	* Split panel
	*
	* @param {Int} index Panel element using zero-based index
	* @param {String} type "horizontal" or "vertical" split
	* @param {Int} size Size of new panel in pixels
	*/
	splitPanel(params) {
		
		if (!params) throw new Error("Invalid params");
		if (params.index === undefined) throw new Error("Missing index param");
		if (!params.type) throw new Error("Missing type param");
		if (!params.size) throw new Error("Missing size param");
		
		var panelEl = this.getPanel(params.index);
		if (!panelEl) throw new Error("Invalid panel index");
		
		//	panel index counter for itterate function
		var _i = -1;
		
		//
		//	recursive function that itterates panels structure
		//	until panel index is matched and splits it
		//
		var itterate = function(panels, index) {
			
			//	only increment panel index when element has no type (is not splitted)
			if (!panels[index].type) _i++;
			
			if (_i == params.index) {
				
				//	NOTE: should not happend, just in case
				if (panels[index].type) throw new Error("Panel is already split");
				
				//
				//	change element to split and create new structure
				//
				panels[index] = {
					type: params.type,
					size: params.size,
					panels: [
						{type: null},
						{type: null}
					]
				};
	
				//
				//	create html elements
				//
				this.renderPanels(panels[index], panelEl.closest('.ms-panel'));
				
			} else {
				
				//	NOTE: should not happend, just in case
				if (!panels[index].type) return;
				
				//	continue itterating structure
				for (let i=0; i<panels[index].panels.length; i++) {
					if (panels[index].panels[i]) itterate(panels[index].panels, i);	
				}
				
			}
		}.bind(this);
		
		if (!this.panels.length) {
			//
			//	handle no-split state
			//
			var  panel = {type:null};
			this.panels.push(panel);
			itterate(this.panels,0);
		} else {
			//
			//	start itterating panels structure
			//
			//	continue itterating structure
				for (let i=0; i<this.panels[0].panels.length; i++) {
					if (this.panels[0].panels[i]) itterate(this.panels[0].panels, i);	
				}	
		}
		
	}
	
	/**
	* Render component
	*
	* @method render
	*/
	render(parentEl) {
		
		//	first check if component alredy exists in the DOM
		if (this.el) {
			this.refresh();
			return;
		}
		
		//	create component wrapper
		this.el = jQuery('<div id="' + this.id + '" class="multisplit"></div>');
		
		//	TODO: border

		//	add css class
		if (this.class) { for (let cssClass of this.class) this.el.addClass(cssClass); }
		
		//	border
		if (this.border) this.el.addClass('border');
		
		//	if custom style, add to element
		this.applyStyle();
		
		if (parentEl)
			this.el.appendTo(parentEl);
		
		this.init();
		
		//	create initial panel
		var panel = $('<div class="ms-panel-content"></div>');
		
		//	create split wrapper
		var panelWrap = $('<div class="ms-panel"></div>');
		this.initializeTabPanel(panel);
		
		panel.appendTo(panelWrap);
		panelWrap.appendTo(this.el);
		
		//	TODO: active tab events
		panel.click(function(e) {
			//	console.log(e);
		});
		
		this._panelList.push(panel);
		
		if (this.panels.length) this.renderPanels(this.panels[0], panelWrap);
		
	}
	
	/*
	*
	*	Render panel [Private]
	*
	*/
	renderPanels(panel, parentPanelWrap) {
		
		//	get existing panel
		var p1 = parentPanelWrap.children('div.ms-panel-content');
		
		//	create second panel
		var p2 = $('<div class="ms-panel-content"></div>');
		this.initializeTabPanel(p2);
		
		//	create split wrappers
		var p1Wrap = $('<div class="ms-panel"></div>');
		var p2Wrap = $('<div class="ms-panel"></div>');
		
		p1.appendTo(p1Wrap);
		p2.appendTo(p2Wrap);
		
		//	append wrapper to parent wrapper container
		p1Wrap.appendTo(parentPanelWrap);
		p2Wrap.appendTo(parentPanelWrap);
		
		this._panelList.push(p2);
		
		//	TODO: active tab events
		p2.click(function(e) {
			//	console.log(e);
		});
		
		switch (panel.type) {
			case 'horizontal':
				
				p1Wrap.addClass('ms-panel-horizontal');
				p2Wrap.addClass('ms-panel-horizontal');
				p1Wrap.css('width', panel.size);
				
				VendorMultiSplit.splitHorizontal({
					el: p1Wrap,
					type: 'h',
					size: panel.size
				});
				
				break;
			case 'vertical':
				
				p1Wrap.addClass('ms-panel-vertical');
				p2Wrap.addClass('ms-panel-vertical');
				p1Wrap.css('height', panel.size);
				
				VendorMultiSplit.splitVertical({
					el: p1Wrap,
					type: 'v',
					size: panel.size
				});
				
				break;
			default:
				throw new Error("Unknown layout type");
		}
		
		if (panel.panels && panel.panels.length == 2) {
			if (panel.panels[0].type) this.renderPanels(panel.panels[0], p1Wrap);
			if (panel.panels[1].type) this.renderPanels(panel.panels[1], p2Wrap);
		}
		if (panel.panels && panel.panels.length == 1) {
			if (panel.panels[0].type) this.renderPanels(panel.panels[0], p1Wrap);
		}
		
		
	}
	
}