import { Container } from './container.js';

/**
 * A specialized container representing the viewable application area (the browser viewport).
 * The Viewport renders itself to the document body, and automatically sizes itself to the size of the browser viewport and manages window resizing. There may only be one Viewport created in a page.
 * Like any Container, a Viewport will only perform sizing and positioning on its child Components if you configure it with a layout.
 *
 * A Common layout used with Viewports is border layout, but if the required layout is simpler, a different layout should be chosen.
 * For example, to simply make a single child item occupy all available space, use fit layout.
 * To display one "active" item at full size from a choice of several child items, use card layout.
 * 
 * The Viewport does not provide scrolling, so child Panels within the Viewport should provide for scrolling if needed using the autoScroll config.
 *
 * @class Viewport
 * @extends Container
 */
export class Viewport extends Container {
    
	/**
	 * @class Viewport
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* DOM element to render component on
		* Note that this property should not be changed
		* 
		* @property text
		* @type {string}
		* @default (selector)body
		*/
		this.el = jQuery('body');
		
		/**
		* Body as parent element
		* Note that this property should not be changed
		* 
		* @property text
		* @type {string}
		* @default 'body'
		*/
		this.parent = 'body';
		
		this.initialize(params);
		
    }
	
	/**
	* Render component
	*
	* @method render
	*/
	render() {

		this.el.addClass('ui-body');
		
		if (typeof this.layout.init != 'function')
			throw new Error("Invalid Layout");
		
		this.layout.init();
		
		//	render all child elements
		for (let item of this.items) {
			this.layout.place(item);
		}
		
	}
	
}