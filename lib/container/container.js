import { Component } from '../component.js';

import { LayoutAbsolute } from '../layout/layouts/absolute.js';
import { LayoutAccordion } from '../layout/layouts/accordion.js';
import { LayoutAuto } from '../layout/layouts/auto.js';
import { LayoutBorder } from '../layout/layouts/border.js';
import { LayoutFieldSet } from '../layout/layouts/fieldset.js';
import { LayoutFit } from '../layout/layouts/fit.js';
import { LayoutHBox } from '../layout/layouts/hbox.js';
import { LayoutVBox } from '../layout/layouts/vbox.js';
import { LayoutPage } from '../layout/layouts/page.js';
import { LayoutTab } from '../layout/layouts/tab.js';

/**
 * Base class for any Ext.Component that may contain other Components. 
 * Containers handle the basic behavior of containing items, namely adding, inserting and removing items.
 *
 * @class Container
 * @extends Component
 */
export class Container extends Component {
    
	/**
	 * @class Container
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Array of components
		* 
		* @property items
		* @type {Array}
		*/
		this.items = [];
		
		/**
		* Container layout
		* 
		* @property layout
		* @type {string}
		* @default 'auto'
		*/
		this.layout = 'auto';
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.items) this.items = params.items;
		if (params.layout) this.layout = params.layout;
		
		for (let i=0; i<this.items.length; i++) {
			let component = this.items[i];
			if (!component.componentManager) component.componentManager = this.componentManager;
			switch (component.xtype) {
				case 'button':
					this.items[i] = new this.componentManager.ui.base.Button(component);
					break;
				case 'container':
					this.items[i] = new this.componentManager.ui.base.Container(component);
					break;
				case 'combobox':
					this.items[i] = new this.componentManager.ui.base.Form.ComboBox(component);
					break;
				case 'formpanel':
					this.items[i] = new this.componentManager.ui.base.Form.FormPanel(component);
					break;
				case 'hmenu':
					this.items[i] = new this.componentManager.ui.base.Menu.HorizontalMenu(component);
					break;
				case 'html':
					this.items[i] = new this.componentManager.ui.base.HtmlFrame(component);
					break;
				case 'input':
					this.items[i] = new this.componentManager.ui.base.Form.Input(component);
					break;
				case 'fieldset':
					this.items[i] = new this.componentManager.ui.base.Form.FieldSet(component);
					break;
				case 'multisplit':
					this.items[i] = new this.componentManager.ui.base.Multisplit(component);
					break;
				case 'panel':
					this.items[i] = new this.componentManager.ui.base.Panel(component);
					break;
				case 'pagepanel':
					this.items[i] = new this.componentManager.ui.base.PagePanel(component);
					break;
				case 'tabpanel':
					this.items[i] = new this.componentManager.ui.base.TabPanel(component);
					break;
				case 'window':
					this.items[i] = new this.componentManager.ui.base.Window(component);
					break;
			}
		}
		
	}
	
	get items() {
		return this._items;
	}
	set items(newValue) {
		
		this._items = newValue;
		
	}
	
	get layout() {
		return this._layout;
	}
	set layout(newValue) {
		
		var config = false;
		if (newValue.config) {
			var config = newValue.config;
			var newValue = newValue.name;
		}
		
		switch(newValue) {
			case 'absolute':
				this._layout = new LayoutAbsolute({
					parent: this
				});
				break;
			case 'accordion':
				this._layout = new LayoutAccordion({
					parent: this,
					config: config
				});
				break;
			case 'border':
				this._layout = new LayoutBorder({
					parent: this,
					config: config
				});
				break;
			case 'column':
				this._layout = new LayoutColumn({
					parent: this
				});
				break;
			case 'fieldset':
				this._layout = new LayoutFieldSet({
					parent: this
				});
				break;
			case 'fit':
				this._layout = new LayoutFit({
					parent: this
				});
				break;
			case 'hbox':
				this._layout = new LayoutHBox({
					parent: this,
					config: config
				});
				break;
			case 'vbox':
				this._layout = new LayoutVBox({
					parent: this,
					config: config
				});
				break;
			case 'page':
				this._layout = new LayoutPage({
					parent: this,
					config: config
				});
				break;
			case 'tab':
				this._layout = new LayoutTab({
					parent: this,
					config: config
				});
				break;
			case 'auto':
			default:
				this._layout = new LayoutAuto({
					parent: this
				});
		}
		
	}
	
	/**
	* Add component to container
	*
	* @method add
	* @param {Component} component Component to be added
	* @param {Int} index Zero based index where the component is added in the list
	*/
	add(params) {
		
		var component = params;
		var index = this.items.length;
		
		if (params.component) component = params.component;
		if (params.index) index = params.index;
		
		this.items.splice(index,0,component);
		
		this.layout.place({
			component: component,
			index: index
		});
		
		this.refresh();	//	TODO: recalc layout structure upon add component
		
	}
	
	/**
	* Refresh container layout and child components
	* This methd is auto-invoked when on layout panel resize from viewport component
	*
	* @method refresh
	*/
	refresh() {
		
		if (!this.el) return false;
		
		this.layout.refresh();//	render all child elements
		
		//	propagate refresh to child components
		for (let item of this.items) {
			item.refresh();
		}
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		
		if (typeof this.layout.init != 'function')
			throw new Error("Invalid Layout");
		
		this.layout.init();
		
		//	render all child elements
		for (let item of this.items) {
			this.layout.place(item);
		}
		
		this.layout.finalize();
		
		//	init all child elements
		for (let item of this.items) {
			item.init();
		}
		
	}
	
}