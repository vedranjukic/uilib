import { Component } from '../../component.js';
import { VendorHorizontalMenu } from '../../vendor/menu/horizontal/horizontalMenu.js';

/**
 * Creates horizontal menu with unlimited sub-menus.
 *
 * @class HorizontalMenu
 * @extends Component
 */
export class HorizontalMenu extends Component {
    
	/**
	 * @class HorizontalMenu
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Data record list
		* Array of data objects (records) that represend dataset
		* 
		* @property data
		* @type {Array}
		*/
		this.items = [];
		
		/**
		* Width of submenu in pixels
		* 
		* @property data
		* @type {int}
		* @default 100
		*/
		this.subMenuWidth = 100;
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.items) this.items = params.items;
		
		if (params.subMenuWidth) this.subMenuWidth = params.subMenuWidth;
		
	}
	
	get items() {
		return this._items;
	}
	set items(newVal) {
		this._items = (Array.isArray(newVal))?newVal:[];
	}
	
	refresh() {
		
		if (!this.el) return false;
		
	}
	
	/**
	* Add menu item
	*
	* @method addItem
	* @param {String} parentItemId Parent menu item element id
	* @param {String} id Menu item id
	* @param {String} label Menu item label
	* @param {Function} onClick Menu item onClick callback
	*/
	addItem(params) {
	
		if (!params) throw new Error("Missing params");
		if (!params.parentItemId) throw new Error("Missing parentItemId param");
		if (!params.label) throw new Error("Missing label param");
		
		var parentItem = $('#' + this.name + '-' + params.parentItemId);		
		
		if (!parentItem.length) throw new Error("Menu parent item not found");
		
		var ul = parentItem.children('ul');
		if (!ul.length) {
			ul = $('<ul>');
			ul.appendTo(parentItem);
		}
		
		var li = $('<li' + ((params.id)?' id="' + this.name + '-' + params.id + '"':'') + '></li>');
			
		var a = $('<a href="#">' + params.label + '</a>');
		
		if (params.onClick) a.on('click', params.onClick);
		
		a.appendTo(li);
		li.appendTo(ul);
		
	}
	
	/**
	* Remove menu item
	*
	* @method removeItem
	* @param {String} id Menu item id
	*/
	removeItem(id) {
	
		if (!id) throw new Error("Missing menu item id param");
		
		var menuItem = $('#' + this.name + '-' + id);
		
		if (!menuItem.length) throw new Error("Menu item not found");
		
		menuItem.remove();
		
	}
	
	/**
	* Remove all submenu item
	*
	* @method removeItem
	* @param {String} id Menu item id
	*/
	removeAllItems(id) {
	
		if (!id) throw new Error("Missing menu item id param");
		
		var menuItem = $('#' + this.name + '-' + id);
		
		if (!menuItem.length) throw new Error("Menu item not found");
		
		var ul = menuItem.children('ul');
		if (!ul.length) return;
		
		ul.children('li').remove();
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		
		var el = $('<ul class="hmenu"></ul>');
		
		function menuSub(items, el, rootName) {
	
			for (let item of items) {
				
				let li = $('<li' + ((item.id)?' id="' + rootName + '-' + item.id + '"':'') + '></li>');
				
				var a = $('<a href="#">' + item.label + '</a>');
		
				if (item.onClick) a.on('click', item.onClick);
			
				a.appendTo(li);
				
				if (item.items) {
					let ul = $('<ul></ul>');
					menuSub(item.items, ul, rootName);	
					ul.appendTo(li);
				}

				li.appendTo(el);
			}
			
		}
		
		if (this._items) 
			menuSub(this._items, el, this.name);
		
		el.appendTo( this.el );

		this.ref = VendorHorizontalMenu.create(this);
		
		this.refresh();
		
	}
	
}