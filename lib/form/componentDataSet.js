import { ComponentFormField } from './componentFormField.js';

/**
 * Extended Base class for all UI components that have data set ability.
 *
 * @class ComponentDataSet
 * @extends ComponentFormField
 */
export class ComponentDataSet extends ComponentFormField {
    
	/**
	 * @class ComponentDataSet
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* DataSet component
		* 
		* @property dataset
		* @type {DataSet}
		*/
		this.dataSet = false;
		
		/**
		* On DataSet data change event handler
		*
		* @event onDataSetChange
		*/
		this.onDataSetChange = function(){ this.refresh(); }.bind(this);
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.dataSet) this.dataSet = params.dataSet;
		
		if (params.onDataSetChange && typeof params.onDataSetChange != 'function') throw new Error("Not a valid function");
		if (params.onDataSetChange) this.onDataSetChange = params.onDataSetChange;
		
	}
	
	get dataSet() {
		return this._dataSet;
	}
	set dataSet(newVal) {

		if (!newVal) return;
		if (newVal.getTypeName() != 'DataSet') throw new Error("Not a valid DataSet object");
		this._dataSet = newVal;
		this._dataSet.hook(this);
		this.refresh();
		
	}
	
	/**
	* Initialize component - bind event handlers
	*
	* @method init
	*/
	init() {
		
		super.init();
		
	}
		
}