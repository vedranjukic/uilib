/**
 * Special helper class used to define validation rules for any componentFormField ancestor.
 * See http://jqueryvalidation.org/ for more info
 *
 * @class FieldValidation
 */
export class FieldValidation {
    
	/**
	 * @class FieldValidation
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {

		/**
		* Makes the element require a credit card number
		* 
		* @property creditcard
		* @type {bool}
		* @default false
		*/
		this.creditcard = (params.creditcard)?params.creditcard:false;
		
		/**
		* Makes the element require a date
		* 
		* @property date
		* @type {bool}
		* @default false
		*/
		this.date = (params.date)?params.date:false;
		
		/**
		* Makes the element require an ISO date
		* 
		* @property dateISO
		* @type {bool}
		* @default false
		*/
		this.dateISO = (params.dateISO)?params.dateISO:false;
		
		/**
		* Makes the element require digits only
		* 
		* @property digits
		* @type {bool}
		* @default false
		*/
		this.digits = (params.digits)?params.digits:false;
		
		/**
		* Makes the element require a valid email
		* 
		* @property email
		* @type {bool}
		* @default false
		*/
		this.email = (params.email)?params.email:false;
		
		/**
		* Requires the element to be the same as another one
		* 
		* @property equalTo
		* @type {Object}
		*/
		this.equalTo = (params.equalTo)?params.equalTo:false;
		
		/**
		* Makes the element require a given minimum
		* 
		* @property min
		* @type {int}
		*/
		this.min = (params.min)?params.min:false;
		
		/**
		* Makes the element require a given minimum length
		* 
		* @property minlength
		* @type {int}
		*/
		this.minlength = (params.minlength)?params.minlength:false;
		
		/**
		* Makes the element require a given maximum
		* 
		* @property max
		* @type {int}
		*/
		this.max = (params.max)?params.max:false;
		
		/**
		* Makes the element require a given maxmimum length
		* 
		* @property maxlength
		* @type {int}
		*/
		this.maxlength = (params.maxlength)?params.maxlength:false;
		
		/**
		* Makes the element require a decimal number
		* 
		* @property number
		* @type {bool}
		*/
		this.number = (params.number)?params.number:false;
		
		/**
		* Makes the element require a given value range
		* 
		* @property range
		* @type {Array}
		*/
		this.range = (params.range)?params.range:false;
		
		/**
		* Makes the element require a given value range
		* 
		* @property rangelength
		* @type {Array}
		*/
		this.rangelength = (params.rangelength)?params.rangelength:false;
		
		/**
		* Makes the element required
		* 
		* @property rangelength
		* @type {bool}
		* @default false
		*/
		this.required = (params.required)?params.required:false;
		
		/**
		* Requests a resource to check the element for validity
		* 
		* @property remote
		* @type {Object}
		*/
		this.remote = (params.remote)?params.remote:false;
		
		/**
		* Makes the element require a valid url
		* 
		* @property url
		* @type {bool}
		* @default false
		*/
		this.url = (params.url)?params.url:false;
		
		/**
		* Message object with same property names as FieldValidation object
		* Each Message property value if defined will override default validation message
		* 
		* @property messages
		* @type {Object}
		*/
		this.messages = (typeof params.messages === 'object')?params.messages:{};
			
    }
	
}