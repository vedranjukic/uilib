import { Panel } from '../panel/panel.js';

/**
 * A container for grouping sets of fields, rendered as a HTML fieldset element. The title config will be rendered as the fieldset's legend.
 * While Fieldsets commonly contain simple groups of fields, they are general Containers and may therefore contain any type of components in their items, including other nested containers.
 * The default layout for the FieldSet's items is 'fit', but it can be configured to use any other layout type.
 *
 * @class FieldSet
 * @extends Panel
 */
export class FieldSet extends Panel {
    
	/**
	 * @class FieldSet
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {

		/**
		* Layout type for FieldSet Panel
		* Should be left default
		* 
		* @property layout
		* @type {string}
		* @default 'fieldset'
		*/
		params.layout = 'fieldset';
		
		super(params);
		
		this.initialize(params);
		
    }
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		super.render(parentEl);
		
	}
	
}