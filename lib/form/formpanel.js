import { Panel } from '../panel/panel.js';
import { Form } from '../vendor/form/form.js';
import { FormValidator } from '../vendor/form/formvalidator.js';

/**
 * FormPanel provides a standard container for forms. It is essentially a standard Panel which automatically creates a BasicForm for managing any componentFormField ancestor objects that are added as descendants of the panel.
 *
 * @class FormPanel
 * @extends Panel
 */
export class FormPanel extends Panel {
    
	/**
	 * @class FormPanel
	 * @constructor
	 *
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* On form submit handler
		*
		* @event onSubmit
		*/
		this.onSubmit = function(){};
		
		/**
		* On form submit validation fail handler
		*
		* @event onSubmitInvalid
		*/
		this.onSubmitInvalid = function(){};
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.onSubmit && typeof params.onSubmit != 'function') 
			throw new Error("Not a valid function");
		
		if (params.onSubmit) 
			
			this.onSubmit = (formFields) => {
			
			//
			//	convert formFields array to object
			//	to make it easier to use in callback function
			//
				
			//	because blank fields are not existent in formField
			//	traverse all field components in the form
			//	and add missing fields to formFields
			
			if (!formFields || !formFields.length) formFields = [];
				
			var fieldComponents = this.getFieldComponents();
			for (let fieldComponent of fieldComponents) {
				let found = false;
				for (let field of formFields) {
					if (field.name == fieldComponent.fieldName) {
						found = true;
						break;
					}
				}
				if (!found)
					formFields.push({
						name: fieldComponent.fieldName,
						value: fieldComponent.value
					});
			}
				
			var fields = {};
			for (let field of formFields) {
				fields[field.name] = field.value;
			}
			params.onSubmit(fields);
		
		};
		
		if (params.onSubmitInvalid && typeof params.onSubmitInvalid != 'function') 
			throw new Error("Not a valid function");
		
		if (params.onSubmitInvalid) 
			
			this.onSubmitInvalid = (formFields) => {
				
			//	because blank fields are not existent in formField
			//	traverse all field components in the form
			//	and add missing fields to formFields
				
			if (!formFields || !formFields.length) formFields = [];
				
			var fieldComponents = this.getFieldComponents();
			for (let fieldComponent of fieldComponents) {
				let found = false;
				for (let field of formFields) {
					if (field.name == fieldComponent.fieldName) {
						found = true;
						break;
					}
				}
				if (!found)
					formFields.push({
						name: fieldComponent.fieldName,
						value: fieldComponent.value
					});
			}
			
			//
			//	convert formFields array to object
			//	to make it easier to use in callback function
			//
			var fields = {};
			for (let field of formFields) {
				fields[field.name] = field.value;
			}
			params.onSubmitInvalid(fields);
		
		};
		
	}
	
	/**
	 * Return all child components ancestors of componentFormFiled recursively
	 *
	 * @method getFieldComponents 
	 */
	getFieldComponents() {
		
		var components = [];
		
		function itterate(items) {
			
			for (let component of items) {
			
				if (component._isComponentFormField) components.push(component);

				if (component.items && component.items.length) itterate(component.items);

			}
			
		}
		
		itterate(this.items);

		return components;
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {

		this.elForm = $(' <form></form> ');
		
		this.elForm.appendTo(parentEl);
		
		super.render(this.elForm);
		
		FormValidator.init(this);
		Form.init(this);
		
		
	}
	
	/**
	 * Submits form data
	 *
	 * @method submit
	*/
	submit() {

		if (this.elForm.valid()) {
			this.elForm.submit();
		} else {
			this.onSubmitInvalid();
		}
		
	}
	
}