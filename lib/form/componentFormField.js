import { ComponentFocus } from '../componentFocus.js';

/**
 * Extended Base class for all UI components that have data field ability.
 *
 * @class ComponentFormField
 * @extends ComponentFocus
 */
export class ComponentFormField extends ComponentFocus {
    
	/**
	 * @class ComponentFormField
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		this._isComponentFormField = true;
		
		/**
		* Data field name
		* 
		* @property fieldName
		* @type {string}
		*/
		this.fieldName = '';
		
		/**
		* Field validation
		* 
		* @property fieldValidation
		* @type {string}
		*/
		this.fieldValidation = false;
		
		/**
		* On data change event handler
		*
		* @event onChange
		*/
		this.onChange = function(){};
		
		/**
		* On key down event handler
		*
		* @event onKeyDown
		*/
		this.onKeyDown = function(){};
		
		/**
		* On key press event handler
		*
		* @event onKeyPress
		*/
		this.onKeyPress = function(){};
		
		/**
		* On key up event handler
		*
		* @event onKeyUp
		*/
		this.onKeyUp = function(){};
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.fieldName) this.fieldName = params.fieldName;
		
		if (params.fieldValidation) this.fieldValidation = params.fieldValidation;
		
		if (params.onChange && typeof params.onChange != 'function') throw new Error("Not a valid function");
		if (params.onChange) this.onChange = params.onChange;
		
		if (params.onKeyDown && typeof params.onKeyDown != 'function') throw new Error("Not a valid function");
		if (params.onKeyDown) this.onKeyDown = params.onKeyDown;
			
		if (params.onKeyPress && typeof params.onKeyPress != 'function') throw new Error("Not a valid function");
		if (params.onKeyPress) this.onKeyPress = params.onKeyPress;
		
		if (params.onKeyUp && typeof params.onKeyUp != 'function') throw new Error("Not a valid function");
		if (params.onKeyUp) this.onKeyUp = params.onKeyUp;
		
	}
	
	/**
	* Initialize component - bind event handlers
	*
	* @method init
	*/
	init() {
		
		super.init();
		
		/**
		* Content change event handler
		*
		* @event onChange
		*/
		this.el.on('change', this.onChange);
		
		/**
		* Key down event handler
		*
		* @event onKeyDown
		*/
		this.el.on('keydown', this.onKeyDown);
		
		/**
		* Key press event handler
		*
		* @event onKeyPress
		*/
		this.el.on('keypress', this.onKeyPress);
		
		/**
		* Key up event handler
		*
		* @event onKeyUp
		*/
		this.el.on('keyup', this.onKeyUp);
		
	}
		
}