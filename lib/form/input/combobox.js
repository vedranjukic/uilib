import { Input } from './input.js';
import { VendorComboBox } from '../../vendor/form/combobox.js';

/**
 * Combo Box component.
 *
 * @class ComboBox
 * @extends Input
 */
export class ComboBox extends Input {
    
	/**
	 * @class ComboBox
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Display field name
		* 
		* @property displayField
		* @type {string}
		*/
		this.displayField = false;
		
		/**
		* Value field name
		* 
		* @property valueField
		* @type {string}
		*/
		this.valueField = false;
		
		/**
		* Field value
		* 
		* @property value
		* @type {String}
		*/
		this.value = false;
		
		/**
		* On dropdown close event handler
		*
		* @event onClose
		*/
		this.onClose = function(){};
		
		/**
		* On dropdown open event handler
		*
		* @event onOpen
		*/
		this.onOpen = function(){};
		
		/**
		* On select item event handler
		*
		* @event onSelect
		*/
		this.onSelect = function(){};
		
		/**
		* On DataSet data change event handler
		* If not set will trigger refresh method by default
		*
		* @event onDataSetChange
		*/
		this.onDataSetChange = function(){ this.refresh(); }.bind(this);
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.displayField) this.displayField = params.displayField;
		if (params.valueField) this.valueField = params.valueField;
		
		if (params.onClose && typeof params.onClose != 'function') throw new Error("Not a valid function");
		if (params.onClose) this.onClose = params.onClose;
		
		if (params.onOpen && typeof params.onOpen != 'function') throw new Error("Not a valid function");
		if (params.onOpen) this.onOpen = params.onOpen;
		
		if (params.onSelect && typeof params.onSelect != 'function') throw new Error("Not a valid function");
		if (params.onSelect) this.onSelect = params.onSelect;
		
		if (params.onDataSetChange && typeof params.onDataSetChange != 'function') throw new Error("Not a valid function");
		if (params.onDataSetChange) this.onDataSetChange = params.onDataSetChange;
		
	}
	
	set displayField(newValue) {
		this._displayField = newValue;
		this.refresh();
	}
	get displayField() {
		return this._displayField;
	}
	
	set valueField(newValue) {
		this._valueField = newValue;
		this.refresh();
	}
	get valueField() {
		return this._valueField;
	}
	
	set value(newValue) {
		this._value = newValue;
		this.refresh();
	}
	get value() {
		return this._value;
	}
	
	/**
	* Refresh component
	*
	* @method refresh
	*/
	refresh() {
		
		if (!this.el) return false;
		
		this.elLabel.html(this.label);
		
		this.el.html('');

		if (this.dataSet) {
			for (let record of this.dataSet.data) {
				if (record[this.displayField] && record[this.valueField]) {
					$('<option value="' + record[this.valueField] + '">' + record[this.displayField] + '</option>').appendTo(this.el);
				}
			}	
		}
		
		this.ref = VendorComboBox.create(this);
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		//	don't inherit render from component ancestor so we don't spam DOM with extra div
		
		//	first check if component alredy exists in the DOM
		if (this.el) {
			this.refresh();
			return false;
		}
		
		//	create component wrapper
		this.el = jQuery('<select id="' + this.id + '" ' + ((this.fieldName)?'name="' + this.fieldName + '"':'') + '></select>');
		
		this.el.change(() => {
			this._value = this.el.val();
		});
		
		if (this.label) this.elLabel = jQuery('<label for="' + this.id + '">' + this.label + '</label>');

		//	add css class
		if (this.class) { for (let cssClass of this.class) this.el.addClass(cssClass); }
		
		//	if custom style, add to element
		this.applyStyle();
		
		if (parentEl) {
			if (this.elLabel) this.elLabel.appendTo(parentEl);
			this.el.appendTo(parentEl);
		}
		
		this.refresh();
		
	}
	
}