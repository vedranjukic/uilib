import { ComponentDataSet } from '../componentDataSet.js';

/**
 * Simple input filed component.
 *
 * @class Input
 * @extends ComponentDataSet
 */
export class Input extends ComponentDataSet {
    
	/**
	 * @class Input
	 * @constructor
	 * @param {Object} params Params object can include any property or event avaliable for this class (see Properties/Events for complete list)
	 */
	constructor(params) {
		
		super(params);
		
		/**
		* Field label
		* 
		* @property label
		* @type {string}
		*/
		this.label = '';
		
		/**
		* Default value
		* 
		* @property default
		* @type {string}
		*/
		this.default = '';
		
		/**
		* Password field
		* 
		* @property password
		* @type {bool}
		*/
		this.password = false;
		
		/**
		* Field value
		* 
		* @property value
		* @type {String}
		*/
		this.value = false;
		
		/**
		* Placeholder text when component value is empty
		* 
		* @property placeholder
		* @type {string}
		*/
		this.placeholder = '';
		
		this.initialize(params);
		
    }
	
	//
	//	protected method
	//	for internal use only
	//
	initialize(params) {
		
		super.initialize(params);
		
		if (params.label) this.label = params.label;
		if (params.default) this.default = params.default;	//	TODO: getter/setter
		if (params.password) this.password = params.password;	//	TODO: getter/setter
		if (params.placeholder) this.placeholder = params.placeholder;	//	TODO: getter/setter
		
	}
	
	set label(newValue) {
		this._label = newValue;
		this.refresh();
	}
	get label() {
		return this._label;
	}
	
	set value(newValue) {
		this._value = newValue;
		this.refresh();
	}
	get value() {
		return this._value;
	}
	
	refresh() {
		
		if (!this.el) return false;
		this.elLabel.html(this.label);
		this.el.val(this.value);
		
	}
	
	/**
	* Render component
	*
	* @method render
	* @param {element} parentEl Parent DOM element to render component into
	*/
	render(parentEl) {
		
		//	don't inherit render from component ancestor so we don't spam DOM with extra div
		
		//	first check if component alredy exists in the DOM
		if (this.el) {
			this.refresh();
			return;
		}
		
		//	create component wrapper
		this.el = jQuery('<input id="' + this.id + '" ' + ((this.fieldName)?'name="' + this.fieldName + '"':'') + ' ' + ((this.password)?'type="password"':'') + ' placeholder="' + this.placeholder + '" value="' + this.default + '" />');
		
		if (this.label) this.elLabel = jQuery('<label for="' + this.id + '">' + this.label + '</label>');
		
		this.el.click(this.onClick);
		
		this.el.change(() => {
			this._value = this.el.val();
		});

		//	add css class
		if (this.class) { for (let cssClass of this.class) this.el.addClass(cssClass); }
		
		//	if custom style, add to element
		this.applyStyle();
		
		if (parentEl) {
			if (this.elLabel) this.elLabel.appendTo(parentEl);
			this.el.appendTo(parentEl);
		}
		
	}
	
}