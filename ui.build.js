System.register("lib/component", [], function() {
  "use strict";
  var __moduleName = "lib/component";
  function require(path) {
    return $traceurRuntime.require("lib/component", path);
  }
  var Component = function Component(params) {
    if (!params)
      params = {};
    this.name = (params.name) ? (params.name) : 'noname';
    CAUI.componentManager.add(this);
  };
  ($traceurRuntime.createClass)(Component, {}, {});
  return {get Component() {
      return Component;
    }};
});
System.register("lib/componentManager", [], function() {
  "use strict";
  var __moduleName = "lib/componentManager";
  function require(path) {
    return $traceurRuntime.require("lib/componentManager", path);
  }
  var ComponentManager = function ComponentManager() {
    this.components = new Map();
    this._lastId = 100;
  };
  ($traceurRuntime.createClass)(ComponentManager, {
    get: function(id) {
      return this.components.get(id);
    },
    register: function(component) {
      component.id = 'c' + this._lastId++;
      this.components.set(component.id, component);
    },
    unregister: function(id) {
      this.components.delete(component.id);
    }
  }, {});
  return {get ComponentManager() {
      return ComponentManager;
    }};
});
System.register("lib/container", [], function() {
  "use strict";
  var __moduleName = "lib/container";
  function require(path) {
    return $traceurRuntime.require("lib/container", path);
  }
  var Component = System.get("lib/component").Component;
  var Container = function Container(params) {
    $traceurRuntime.superConstructor($Container).call(this, params);
    if (!params)
      params = {};
  };
  var $Container = Container;
  ($traceurRuntime.createClass)(Container, {}, {}, Component);
  return {get Container() {
      return Container;
    }};
});
System.register("ui", [], function() {
  "use strict";
  var __moduleName = "ui";
  function require(path) {
    return $traceurRuntime.require("ui", path);
  }
  var Component = System.get("lib/component").Component;
  var ComponentManager = System.get("lib/componentManager").ComponentManager;
  var Container = System.get("lib/container").Container;
  var UI = function UI() {
    this.componentManager = new ComponentManager();
  };
  ($traceurRuntime.createClass)(UI, {}, {});
  return {get UI() {
      return UI;
    }};
});
System.get("ui" + '');
