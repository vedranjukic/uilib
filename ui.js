// traceur.options.experimental = true;

import { Button } from './lib/button/button.js';

import { Container } from './lib/container/container.js';

import { ComboBox } from './lib/form/input/combobox.js';

import { Component } from './lib/component.js';
import { ComponentFormField } from './lib/form/componentFormField.js';
import { ComponentFocus } from './lib/componentFocus.js';
import { ComponentManager } from './lib/componentManager.js';

import { DataSet } from './lib/data/dataset.js';

import { FieldSet } from './lib/form/fieldset.js';
import { FieldValidation } from './lib/form/fieldvalidation.js';
import { FormPanel } from './lib/form/formpanel.js';

import { HtmlFrame } from './lib/basic/htmlframe.js';

import { HorizontalMenu } from './lib/menu/horizontal/horizontalMenu.js';

import { Input } from './lib/form/input/input.js';

import { Layout } from './lib/layout/layout.js';
import { LayoutAbsolute } from './lib/layout/layouts/absolute.js';
import { LayoutAccordion } from './lib/layout/layouts/accordion.js';
import { LayoutAuto } from './lib/layout/layouts/auto.js';
import { LayoutBorder } from './lib/layout/layouts/border.js';
import { LayoutColumn } from './lib/layout/layouts/column.js';
import { LayoutFieldSet } from './lib/layout/layouts/fieldset.js';
import { LayoutFit } from './lib/layout/layouts/fit.js';
import { LayoutHBox } from './lib/layout/layouts/hbox.js';
import { LayoutVBox } from './lib/layout/layouts/vbox.js';
import { LayoutPage } from './lib/layout/layouts/page.js';
import { LayoutTab } from './lib/layout/layouts/tab.js';

import { Multisplit } from './lib/container/multisplit.js';

import { Panel } from './lib/panel/panel.js';
import { PagePanel } from './lib/panel/pagepanel.js';
import { TabPanel } from './lib/panel/tabpanel.js';

import { TreePanel } from './lib/tree/treepanel.js';

import { Viewport } from './lib/container/viewport.js';

import { Window } from './lib/window/window.js';

export class CAWebUI {
	
	constructor() {
		
		this.base = {
		
			Button: Button,
			Container: Container,
			Component: Component,
			ComponentFocus: ComponentFocus,
			ComponentManager: ComponentManager,
			Data: {
				DataSet: DataSet	
			},
			Form: {
				ComboBox: ComboBox,
				ComponentFormField: ComponentFormField,
				FieldSet: FieldSet,
				FieldValidation: FieldValidation,
				FormPanel: FormPanel,
				Input: Input,
			},
			HtmlFrame: HtmlFrame,
			Layout: Layout,
			Layouts: {
				Absolute: LayoutAbsolute,
				Accordion: LayoutAccordion,
				Auto: LayoutAuto,
				Border: LayoutBorder,
				Column: LayoutColumn,
				FieldSet: LayoutFieldSet,
				Fit: LayoutFit,
				HBox: LayoutHBox,
				VBox: LayoutVBox,
				Page: LayoutPage,
				Tab: LayoutTab,
			},
			Menu: {
				HorizontalMenu: HorizontalMenu	
			},
			Multisplit: Multisplit,
			Panel: Panel,
			PagePanel: PagePanel,
			TabPanel: TabPanel,
			TreePanel: TreePanel,
			Viewport: Viewport,
			Window: Window
			
		};
		
		this.componentManager = new ComponentManager(this);
		
	}
	
}

window.CAWebUI = new CAWebUI();